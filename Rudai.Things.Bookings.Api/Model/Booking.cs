﻿using System;
namespace Rudai.Things.Bookings.Api.Models
{
    public class Booking : Schema.NET.Reservation
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }


    }
}
