﻿using Microsoft.EntityFrameworkCore;
using Rudai.Things.Bookings.Api.Models;

namespace Rudai.Things.Bookings.Api.Models
{
    public class BookingsContext : DbContext
    {
        public BookingsContext(DbContextOptions<BookingsContext> options)
            :base(options)
        {

        }
        public DbSet<Booking> Bookings { get; set; }
    }

}
