﻿using Microsoft.AspNetCore.Identity;
using System;

namespace IdentityProvider.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public DateTime DateOfBirth { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string EmailAddress { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }
    }
}
