﻿using Microsoft.EntityFrameworkCore;
using Rudai.Things.Tours.Api.Models;

namespace Rudai.Things.Tours.Api.Data
{
    public class ToursContext : DbContext
    {
        public ToursContext() : base()
        {

        }
        public ToursContext(DbContextOptions<ToursContext> options)
            :base(options)
        {

        }
        public DbSet<Tour> Tours { get; set; }
        public DbSet<TouristAttraction> TouristAttractions { get; set; }
        public DbSet<TouristDestination> TouristDestinations { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

    }

}
