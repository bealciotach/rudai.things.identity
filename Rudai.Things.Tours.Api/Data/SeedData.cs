﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Rudai.Things.Tours.Api.Models;
using System.Collections.Generic;

namespace Rudai.Things.Tours.Api.Data
{
    public class SeedData
    {
        public static void EnsureSeedData(string connectionString)
        {
            var services = new ServiceCollection();
            services.AddLogging();

            services.AddDbContext<ToursContext>(options =>
               options.UseSqlServer(connectionString));

            using (var serviceProvider = services.BuildServiceProvider())
            {
                using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var context = scope.ServiceProvider.GetService<ToursContext>();

                    IList<Tour> defaultTours = new List<Tour>();

                    defaultTours.Add(new Tour() { Name = "Tour 1", Description = "First Tour" });
                    defaultTours.Add(new Tour() { Name = "Tour 2", Description = "Second Tour" });
                    defaultTours.Add(new Tour() { Name = "Tour 3", Description = "Third Tour" });

                    context.Tours.AddRange(defaultTours);

                    //base.Seed(context);
                    context.Database.Migrate();
                }
            }
        }
    }
}
