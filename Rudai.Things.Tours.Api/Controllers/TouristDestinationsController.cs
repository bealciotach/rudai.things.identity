﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Rudai.Things.Tours.Api.Data;
using Rudai.Things.Tours.Api.Models;

namespace Rudai.Things.Tours.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TouristDestinationsController : ControllerBase
    {
        private readonly ToursContext _context;

        public TouristDestinationsController(ToursContext context)
        {
            _context = context;
        }

        // GET: api/TouristDestinations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TouristDestination>>> GetTouristDestinations()
        {
            return await _context.TouristDestinations.Include(td => td.TouristAttractions).ToListAsync();
        }

        // GET: api/TouristDestinations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TouristDestination>> GetTouristDestination(Guid id)
        {
            var touristDestination = await _context.TouristDestinations.FindAsync(id);

            if (touristDestination == null)
            {
                return NotFound();
            }

            return touristDestination;
        }

        // PUT: api/TouristDestinations/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTouristDestination(Guid id, TouristDestination touristDestination)
        {
            if (id != touristDestination.Id)
            {
                return BadRequest();
            }

            _context.Entry(touristDestination).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TouristDestinationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TouristDestinations
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<TouristDestination>> PostTouristDestination(TouristDestination touristDestination)
        {
            _context.TouristDestinations.Add(touristDestination);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTouristDestination", new { id = touristDestination.Id }, touristDestination);
        }

        // DELETE: api/TouristDestinations/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TouristDestination>> DeleteTouristDestination(Guid id)
        {
            var touristDestination = await _context.TouristDestinations.FindAsync(id);
            if (touristDestination == null)
            {
                return NotFound();
            }

            _context.TouristDestinations.Remove(touristDestination);
            await _context.SaveChangesAsync();

            return touristDestination;
        }

        private bool TouristDestinationExists(Guid id)
        {
            return _context.TouristDestinations.Any(e => e.Id == id);
        }
    }
}
