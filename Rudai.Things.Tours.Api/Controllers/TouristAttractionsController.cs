﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Rudai.Things.Tours.Api.Data;
using Rudai.Things.Tours.Api.Models;

namespace Rudai.Things.Tours.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TouristAttractionsController : ControllerBase
    {
        private readonly ILogger<TouristAttractionsController> _logger;

        private readonly ToursContext _context;

        public TouristAttractionsController(ToursContext context)
        {
            _context = context;
        }

        // GET: api/TouristAttractions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TouristAttraction>>> GetTouristAttractions()
        {
            return await _context.TouristAttractions.ToListAsync();
        }

        // GET: api/TouristAttractions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TouristAttraction>> GetTouristAttraction(Guid id)
        {
            var touristAttraction = await _context.TouristAttractions.FindAsync(id);

            if (touristAttraction == null)
            {
                return NotFound();
            }

            return touristAttraction;
        }

        // PUT: api/TouristAttractions/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTouristAttraction(Guid id, TouristAttraction touristAttraction)
        {
            if (id != touristAttraction.Id)
            {
                return BadRequest();
            }

            _context.Entry(touristAttraction).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TouristAttractionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TouristAttractions
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<TouristAttraction>> PostTouristAttraction(TouristAttraction touristAttraction)
        {
            _context.TouristAttractions.Add(touristAttraction);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTouristAttraction", new { id = touristAttraction.Id }, touristAttraction);
        }

        // DELETE: api/TouristAttractions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TouristAttraction>> DeleteTouristAttraction(Guid id)
        {
            var touristAttraction = await _context.TouristAttractions.FindAsync(id);
            if (touristAttraction == null)
            {
                return NotFound();
            }

            _context.TouristAttractions.Remove(touristAttraction);
            await _context.SaveChangesAsync();

            return touristAttraction;
        }

        private bool TouristAttractionExists(Guid id)
        {
            return _context.TouristAttractions.Any(e => e.Id == id);
        }
    }
}
