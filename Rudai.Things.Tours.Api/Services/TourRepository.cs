﻿using Rudai.Things.Tours.Api.Core;
using Rudai.Things.Tours.Api.Data;
using Rudai.Things.Tours.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Tours.Api.Services
{
    public class TourRepository : BaseRepository<Tour>, ITourRepository
    {
        protected readonly ToursContext _context;
        public TourRepository(ToursContext context)
            :base (context)
        {
            _context = context;
        }
    }
}
