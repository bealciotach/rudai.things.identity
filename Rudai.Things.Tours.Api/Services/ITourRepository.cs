﻿using Rudai.Things.Tours.Api.Core;
using Rudai.Things.Tours.Api.Models;

namespace Rudai.Things.Tours.Api.Services
{
    public interface ITourRepository: IBaseRepository<Tour>
    {
    }
}
