﻿using GraphQL.Types;
using GraphQL.Utilities.Federation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Rudai.Things.Tours.Api.Core;
using Rudai.Things.Tours.Api.Data;
using Rudai.Things.Tours.Api.Models;
using Rudai.Things.Tours.Api.Models.Graph;
using Rudai.Things.Tours.Api.Services;
using System;

namespace Rudai.Things.Tours.Api.Config
{
    public static class ToursQLConfiguration
    {
        /// <summary>
        /// Configures the service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="configuration">The configuration.</param>
        public static void ConfigureService(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient(typeof(IBaseRepository<Tour>), typeof(BaseRepository<Tour>));          
            services.AddTransient<ITourRepository, TourRepository>();

            // query mutation subscription
            services.AddSingleton<ToursQuery>();
            //services.AddSingleton<TourTypeMutation>();

            // types
            services.AddSingleton<TourType>();
            services.AddSingleton<TouristAttractionType>();
            services.AddSingleton<TouristDestnationType>();

            // todo remove ExposeExceptions in prod
            // Register GraphQL services
            //services.AddGraphQL(options =>
            //{
            //    options.EnableMetrics = true;
            //    options.ExposeExceptions = true;
            //});
            // Register DepedencyResolver; this will be used when a GraphQL type needs to resolve a dependency
            // services.AddSingleton<IServiceProvider>(c => new FuncServiceProvider(type => c.GetRequiredService(type)));

            services.AddTransient<ISchema>(s =>
            {

                var tourRepository = s.GetRequiredService<TourRepository>();

                return FederatedSchema.For(@"
                        extend type Query {
                            Tour(Id: Guid): Tour
                            Tours: [Tour]
                            TouristAttractions: [TouristAttraction]
                            TouristDestinations: [TouristDestination]
                        }
                    
                        type Tour @key(fields: ""id"") {
                            id: Guid!
                            description: String
                            name: String
                            imageUri: String
                            touristAttractions: [TouristAttraction]
                        }

                        type TouristAttraction @key(fields: ""id""){
                            id: Guid! @external
                            type: String
                            availableLanguage: String
                            touristType: String
                            address: String
                            touristDestinationId: Guid
                        }

                        type TouristDestination @key(fields: ""id""){
                            id: Guid! @external
                            type: String
                            availableLanguage: String
                            touristType: String
                            address: String
                            touristAttractions: [TouristAttraction]                            
                        }
                        
                    ", _ =>
                {
                    _.ServiceProvider = s;
                    _.Types.Include<ToursQuery>();
                    _.Types.For("TouristAttraction").ResolveReferenceAsync(context =>
                    {
                        Console.WriteLine("################ resolving TouristAttractions for Tour #############");
                        var clientId = (Guid)context.Arguments["id"];
                        Console.WriteLine("########### WHAT id " + clientId + "###############");

                        return tourRepository.GetAll();

                    });

                });
            });
        }
    }
}
