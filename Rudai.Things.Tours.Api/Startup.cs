using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Rudai.Things.Tours.Api.Config;
using Rudai.Things.Tours.Api.Core;
using Rudai.Things.Tours.Api.Data;
using Rudai.Things.Tours.Api.Helpers;
using Rudai.Things.Tours.Api.Models;
using Rudai.Things.Tours.Api.Services;

namespace Rudai.Things.Tours.Api
{
    public class Startup
    {
        public IWebHostEnvironment Environment { get; }
        public IConfiguration Configuration { get; }

        public Startup(IWebHostEnvironment environment, IConfiguration configuration)
        {
            Environment = environment;
            Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddHttpContextAccessor();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<ContextServiceLocator>();

            services.AddDbContext<ToursContext>(options => 
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            //EntityFrameworkConfiguration.ConfigureService(services, Configuration);
            services.AddTransient(typeof(IBaseRepository<Tour>), typeof(BaseRepository<Tour>));          
            services.AddTransient<TourRepository>();
            GraphQLConfiguration.ConfigureService(services, Configuration);

            services.AddControllers().AddNewtonsoftJson(options =>
            options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            // not recommended for production - you need to store your key material somewhere secure
            //builder.AddDeveloperSigningCredential();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseGraphQLPlayground();

        }
    }
}
