﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Tours.Api.Models
{
    public class TouristDestination
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public IEnumerable<TouristAttraction> TouristAttractions { get; set; }
    }
}
