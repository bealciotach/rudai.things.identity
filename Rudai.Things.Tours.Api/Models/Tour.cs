﻿using System;
using System.Collections.Generic;

namespace Rudai.Things.Tours.Api.Models
{
    public class Tour
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Gets the name of the type as specified by schema.org.
        /// </summary>
        public string Type { get; private set; } = "Tour";

        /// <summary>
        /// The name of the item.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A description of the item.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
        /// </summary>
        public string AdditionalTypeUri { get; set; }

        /// <summary>
        /// An alias for the item.
        /// </summary>
        public string AlternateName { get; set; }

        /// <summary>
        /// A sub property of description. A short description of the item used to disambiguate from other, similar items. Information from other properties (in particular, name) may be necessary for the description to be useful for disambiguation.
        /// </summary>
        public string DisambiguatingDescription { get; set; }


        /// <summary>
        /// An image of the item. This can be a &lt;a class="localLink" href="http://schema.org/URL"&gt;URL&lt;/a&gt; or a fully described &lt;a class="localLink" href="http://schema.org/ImageObject"&gt;ImageObject&lt;/a&gt;.
        /// </summary>
        public string ImageUri { get; set; }


        /// <summary>
        /// URL of the item.
        /// </summary>
        public string UrlItemUrl { get; set; }
        public IEnumerable<TouristAttraction> TouristAttractions { get; set; }

    }
}
