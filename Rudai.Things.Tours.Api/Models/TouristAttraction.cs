﻿using Schema.NET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Tours.Api.Models
{
    public class TouristAttraction
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Gets the name of the type as specified by schema.org.
        /// </summary>
        public string Type { get; private set; } = "TouristAttraction";
        /// <summary>
        /// The name of the item.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A description of the item.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// A language someone may use with or at the item, service or place. Please use one of the language codes from the &lt;a href="http://tools.ietf.org/html/bcp47"&gt;IETF BCP 47 standard&lt;/a&gt;. See also &lt;a class="localLink" href="http://schema.org/inLanguage"&gt;inLanguage&lt;/a&gt;
        /// </summary>
        public string AvailableLanguage { get; set; }

        /// <summary>
        /// Attraction suitable for type(s) of tourist. eg. Children, visitors from a particular country, etc.
        /// </summary>
        public string TouristType { get; set; }

        public string Address
        {
            get; set;
        }

        public Guid TouristDestinationId { get; set; }
        public TouristDestination TouristDestination { get; set; }
    }
}
