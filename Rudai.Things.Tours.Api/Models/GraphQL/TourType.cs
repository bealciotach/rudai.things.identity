﻿using GraphQL.Types;
using Rudai.Things.Tours.Api.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Tours.Api.Models.Graph
{
    public class TourType : ObjectGraphType<Tour>
    {
        public TourType(ContextServiceLocator contextServiceLocator)
        {
            Field<GuidGraphType>("id", resolve: context => context.Source.Id);
            Field(x => x.Type).Description("The Type");
            Field(x => x.Name).Description("The Name");
            Field(x => x.Description).Description("Description");
            Field(x => x.DisambiguatingDescription).Description("The DisambiguatingDescription");
            Field(x => x.ImageUri).Description("The ImageUri");
            Field(x => x.AdditionalTypeUri).Description("The AdditionalTypeUri");
            Field(x => x.AlternateName).Description("The AlternateName");
            Field(x => x.TouristAttractions).Description("The Secondary colour of the Theme");

            //field<themetype>(
            //   "theme",
            //   arguments: new queryarguments(new queryargument<stringgraphtype> { name = "id" }),
            //   resolve: context => contextservicelocator.clientrepository.getclientbyid(guid.parse(context.getargument<string>("themeid")))
            //);
        }
    }
}
