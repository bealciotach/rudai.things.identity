﻿
using GraphQL;
using GraphQL.Types;
using Rudai.Things.Tours.Api.Helpers;
using Rudai.Things.Tours.Api.Models.Graph;
using System;

namespace Rudai.Things.Tours.Api.Models.Graph
{
    /// <summary>
    /// ObjectGraphType ToursQuery
    /// </summary>
    /// <description>ToursQuery Query fields</description>
    public class ToursQuery : ObjectGraphType<Tour>
    {
        public ToursQuery(ContextServiceLocator contextServiceLocator)
        {
            Field<ListGraphType<TourType>>(
                "tours",
                // arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "cursor" }),
                resolve: context => contextServiceLocator.ToursContext.Tours);

            Field<ListGraphType<TourType>>(
                "touristdestinations",
                // arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "cursor" }),
                resolve: context => contextServiceLocator.ToursContext.TouristDestinations);

            Field<ListGraphType<TourType>>(
                "touristattractions",
                // arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "cursor" }),
                resolve: context => contextServiceLocator.ToursContext.TouristAttractions);

            //Field<ThemeType>(
            //    "theme",
            //    "A ClientTheme whose id matches",
            //    arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "id", Description = "" }),
            //    resolve: context => contextServiceLocator.ClientRepository.GetThemeByClientId(Guid.Parse(context.GetArgument<string>("id"))));

            //Field<ListGraphType<ThemeType>>(
            //    "themes",
            //    arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "cursor" }),
            //    resolve: context => contextServiceLocator.ClientRepository.GetAllThemes());

        }
    }
}


