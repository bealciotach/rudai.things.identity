﻿using GraphQL.Types;
using Rudai.Things.Tours.Api.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Tours.Api.Models.Graph
{
    public class TouristDestnationType : ObjectGraphType<TouristDestination>
    {
        public TouristDestnationType(ContextServiceLocator contextServiceLocator)
        {
            Field<GuidGraphType>("id", resolve: context => context.Source.Id);
            Field(x => x.Name).Description("The Name");
            Field(x => x.Description).Description("Description");
            Field(x => x.TouristAttractions).Description("The TouristAttractions");
        }
    }
}
