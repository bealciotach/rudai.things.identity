﻿using GraphQL.Types;
using Rudai.Things.Tours.Api.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Tours.Api.Models.Graph
{
    public class TouristAttractionType : ObjectGraphType<TouristAttraction>
    {
        public TouristAttractionType(ContextServiceLocator contextServiceLocator)
        {
            Field<GuidGraphType>("id", resolve: context => context.Source.Id);
            Field(x => x.Type).Description("The Type");
            Field(x => x.Name).Description("The Name");
            Field(x => x.Description).Description("Description");
            Field(x => x.Address).Description("The Address");
            Field(x => x.TouristType).Description("The TouristType");
            Field(x => x.TouristDestinationId).Description("The TouristDestinationId");
            Field(x => x.TouristDestination).Description("The TouristDestination");
        }
    }
}
