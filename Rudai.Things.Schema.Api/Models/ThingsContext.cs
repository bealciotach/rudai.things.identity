﻿using Microsoft.EntityFrameworkCore;
using Schema.NET;

namespace Rudai.Things.EntitySchema.Api.Models
{
    public class ThingsContext : DbContext
    {
        public ThingsContext(DbContextOptions<ThingsContext> options)
            :base(options)
        {

        }
        public DbSet<Thing> Things { get; set; }
    }

}
