
1. IdentityProvider must be running - local dev: port = https://localhost:5000
2. serviceList: [
    { name: "accounts", url: "http://localhost:4001/graphql" },
    //{ name: "addresses", url: "http://localhost:9090/graphql" },
    //{ name: "basket", url: "http://localhost:8085/graphql" },
    { name: "reviews", url: "http://localhost:4002/graphql" },
    { name: "products", url: "http://localhost:4003/graphql" },
    { name: "inventory", url: "http://localhost:4004/graphql" },
    //{ name: "things", url: "http://localhost:7070/graphql" },
    //{ name: "accounts", url: "http://localhost:8082/graphql" }
    // { name: "schedule", url:"https://localhost:8001" }
  ],
});