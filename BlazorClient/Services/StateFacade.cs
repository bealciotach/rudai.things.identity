﻿using Fluxor;
using Microsoft.Extensions.Logging;
using Rudai.Things.Models.Shared;
using Rudai.Things.Identity.BlazorClient.Store.Bookings.Actions;
using Rudai.Things.Identity.BlazorClient.Store.Tours.Actions;
using Rudai.Things.Identity.BlazorClient.Store.UI.Actions;
using Rudai.Things.Identity.BlazorClient.Store.WeatherForecasts.Actions;
using System;
using MatBlazor;
using Rudai.Things.Identity.BlazorClient.Store.Schedules.Actions;

namespace Rudai.Things.Identity.BlazorClient.Services
{
    public class StateFacade
    {
        private readonly ILogger<StateFacade> _logger;
        private readonly IDispatcher _dispatcher;

        public StateFacade(ILogger<StateFacade> logger, IDispatcher dispatcher) =>
            (_logger, _dispatcher) = (logger, dispatcher);

        #region Load methods

        public void LoadTours()
        {
            _logger.LogInformation("Issuing action to load tours...");
            _dispatcher.Dispatch(new GetToursAction());
        }

        public void LoadBookings()
        {
            _logger.LogInformation("Issuing action to load bookings...");
            _dispatcher.Dispatch(new GetBookingsAction());
        }

        public void LoadWeatherForecast()
        {
            _logger.LogInformation("Issuing action to load weather...");
            _dispatcher.Dispatch(new GetWeatherForcastsAction());
        }

        public void LoadSchedules()
        {
            _logger.LogInformation("Issuing action to load schedules...");
            _dispatcher.Dispatch(new GetSchedulesAction());
        }

        #endregion

        #region Bookings
        public void AddBooking(Guid id, string title, string description, DateTime tourDate)
        {
            _logger.LogInformation("Issuing action to add tour...");
            Booking booking = new Booking(id, title, description, tourDate);
            _dispatcher.Dispatch(new AddBookingAction(booking));
            _logger.LogInformation("Issued action to add Booking...");

        }
        public void UpdateBooking(Guid id, string title, string description, DateTime tourDate)
        {
            _logger.LogInformation("Issuing action to update booking...");
            Booking booking = new Booking(id, title, description, tourDate);
            _dispatcher.Dispatch(new UpdateBookingAction(booking));
        }

        public void SelectBooking(Booking booking)
        {
            _logger.LogInformation("Issuing action to select booking...");

            _dispatcher.Dispatch(new SelectBookingAction(booking));
        }

        #endregion

        #region Schedules
        public void AddSchedule(Guid id, string title, string description, DateTime scheduleStartDate)
        {
            _logger.LogInformation("Issuing action to add Schedule...");
            Schedule item = new Schedule();
           // _dispatcher.Dispatch(new AddScheduleAction(item));
            _logger.LogInformation("Issued action to add Schedule...");

        }
        public void UpdateSchedule(Guid id, string title, string description, DateTime tourDate)
        {
            _logger.LogInformation("Issuing action to update Schedule...");
            Schedule item = new Schedule();
            //_dispatcher.Dispatch(new UpdateBookingAction(item));
            _logger.LogInformation("Issued action to update Schedule...");

        }

        public void SelectSchedule(Schedule schedule)
        {
            _logger.LogInformation("Issuing action to select Schedule...");

           //_dispatcher.Dispatch(new SelectBookingAction(schedule));
            _logger.LogInformation("Issued action to select Schedule...");

        }

        #endregion

        #region Tours

        public void AddTour(Guid id, string title, string description, DateTime tourDate)
        {
            _logger.LogInformation("Issuing action to add tour...");
            Tour tour = new Tour(id, title, description, tourDate);
            _dispatcher.Dispatch(new AddTourAction(tour));
            _logger.LogInformation("Issued action to add tour...");

        }

        public void UpdateTour(Guid id, string title, string description, DateTime tourDate)
        {
            _logger.LogInformation("Issuing action to update tour...");
            Tour tour = new Tour(id, title, description, tourDate);
            _dispatcher.Dispatch(new UpdateTourAction(tour));
        }

        public void SelectTour(Tour tour)
        {
            _logger.LogInformation("Issuing action to select tour...");

            _dispatcher.Dispatch(new SelectTourAction(tour));
        }
        #endregion

        #region UI
        public void ToggleLeftDrawer(bool isLeftDrawerOpen)
        {
            _logger.LogInformation("Issuing action to select tour...");

            _dispatcher.Dispatch(new ToggleLeftDrawerAction(isLeftDrawerOpen));
        }

        public void SwitchTheme(MatTheme theme)
        {
            _logger.LogInformation("Issuing action to switch theme...");

            _dispatcher.Dispatch(new SwitchThemeAction(theme));
        }
        #endregion
    }
}
