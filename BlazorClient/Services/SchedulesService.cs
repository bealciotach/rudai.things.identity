﻿using Rudai.Things.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Services
{
	public interface ISchedulesService
	{
		Task<Schedule[]> GetItemsAsync();
		Task<Schedule[]> AddItemAsync(Schedule item);
		Task<Schedule[]> UpdateItemAsync(Schedule item);
	}

	public class SchedulesService : ISchedulesService
	{

		private static List<Schedule> Schedules = new List<Schedule>()
		{
			new Schedule()
			{
				Id = Guid.NewGuid(),
				ByDay = "Monday",
				ByMonth = "July",
				ByMonthDay = "Monday",
				ByMonthWeek = "1",
				Duration = 2
			},
			new Schedule()
			{
				Id = Guid.NewGuid(),
				ByDay = "Monday",
				ByMonth = "July",
				ByMonthDay = "Monday",
				ByMonthWeek = "1",
				Duration = 2
			}
		};

		public async Task<Schedule[]> GetItemsAsync()
		{
			await Task.Delay(1000);
			return Schedules
				.ToArray();
		}
		public async Task<Schedule[]> AddItemAsync(Schedule item)
		{
			await Task.Delay(1000);
			Schedules.Add(item);
			return Schedules
				.ToArray();
		}

		public async Task<Schedule[]> UpdateItemAsync(Schedule item)
		{
			await Task.Delay(10);
			var update = Schedules.Where(t => t.Id != item.Id).ToList();
			update.Add(item);
			return update.ToArray();
		}
	}
}
