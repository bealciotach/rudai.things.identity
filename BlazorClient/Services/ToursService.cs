﻿using Rudai.Things.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Services
{
    public interface IToursService
    {
		Task<Tour[]> GetToursAsync();
		Task<Tour[]> AddTourAsync(Tour tour);
		Task<Tour[]> UpdateTourAsync(Tour tour);
	}
	public class ToursService : IToursService
	{

		private static List<Tour> Tours = new List<Tour>()
		{
			new Tour(Guid.NewGuid(),"Tour 1 0101", "Tour 1 Description", DateTime.Now.AddDays(5)),
			new Tour(Guid.NewGuid(),"Tour 2 1010", "Tour 2 Description", DateTime.Now.AddDays(7)),
			new Tour(Guid.NewGuid(),"Tour 3 100", "Tour 3 Description", DateTime.Now.AddDays(9)),
			new Tour(Guid.NewGuid(),"Tour 4 100", "Tour 4 Description", DateTime.Now.AddDays(2)),
			new Tour(Guid.NewGuid(),"Tour 5 100", "Tour 5 Description", DateTime.Now.AddDays(3)),
			new Tour(Guid.NewGuid(),"Tour 6 100", "Tour 6 Description", DateTime.Now.AddDays(6))
		};

		public async Task<Tour[]> GetToursAsync()
		{
			await Task.Delay(1000);
			return Tours
				.ToArray();
		}
		public async Task<Tour[]> AddTourAsync(Tour tour)
		{
			await Task.Delay(1000);
			Tours.Add(tour);
			return Tours
				.ToArray();
		}

		public async Task<Tour[]> UpdateTourAsync(Tour tour)
		{
			await Task.Delay(10);
			var update = Tours.Where(t => t.Id != tour.Id).ToList();
			update.Add(tour);
			return update.ToArray();
		}
	}
}
