﻿using Fluxor;
using MatBlazor;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Rudai.Things.Identity.BlazorClient.Services;
using Rudai.Things.Identity.BlazorClient.Store.UI;
using System;

namespace Rudai.Things.Identity.BlazorClient.Shared
{
    public partial class ThemeSwitcher
    {

        /// <summary>
        /// UIState theme, UI open/closed state
        /// </summary>
        [Inject]
        private IState<UIState> UIState { get; set; }

        /// <summary>
        /// StateFacade
        /// </summary>
        [Inject]
        private StateFacade StateFacade { get; set; }

        /// <summary>
        /// OpenSwitcherMenuButton BaseMatIconButton
        /// </summary>
        public BaseMatIconButton OpenSwitcherMenuButton;

        /// <summary>
        /// BaseMatMenu ThemeMenu
        /// </summary>
        public BaseMatMenu ThemeMenu;

        /// <summary>
        /// MatTheme SelectedTheme
        /// </summary>
        [Parameter]
        public MatTheme SelectedTheme { get; set; }

        protected override void OnInitialized()
        {
            base.OnInitialized();
            SelectedTheme = UIState.Value.Themes[0];
        }

        /// <summary>
        /// SelectTheme selects the Theme and dispatches action
        /// </summary>
        /// <param name="e">MouseEventArgs</param>
        protected void SelectTheme(MouseEventArgs e)
        {
            Console.WriteLine(string.Format("{0}={1}", "MouseEventArgs", e));

            if (SelectedTheme == UIState.Value.Themes[0])
            {
                StateFacade.SwitchTheme(UIState.Value.Themes[1]);
            }
            else
            {
                StateFacade.SwitchTheme(UIState.Value.Themes[0]);
            }

            StateHasChanged();

        }

        /// <summary>
        /// OnSelectTheme selects the Theme and dispatches action
        /// </summary>
        /// <param name="e">ChangeEventArgs</param>
        protected void OnSelectTheme(ChangeEventArgs e)
        {
            Console.WriteLine(string.Format("{0}={1}", "ChangeEventArgs", e.Value));
            if (SelectedTheme == UIState.Value.Themes[0])
            {
                StateFacade.SwitchTheme(UIState.Value.Themes[1]);
            }
            else
            {
                StateFacade.SwitchTheme(UIState.Value.Themes[0]);
            }

            StateHasChanged();

        }
        /// <summary>
        /// OpenSwitcher opens the Menu
        /// </summary>
        /// <param name="e">MouseEventArgs</param>
        protected void OpenSwitcher(MouseEventArgs e)
        {
            ThemeMenu.OpenAsync(OpenSwitcherMenuButton.Ref);
        }
    }
}
