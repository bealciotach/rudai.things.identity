﻿using Fluxor;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Rudai.Things.Identity.BlazorClient.Store.Bookings;
using Rudai.Things.Identity.BlazorClient.Store.Counter;
using Rudai.Things.Identity.BlazorClient.Store.Tours;
using Rudai.Things.Identity.BlazorClient.Store.UI;
using Rudai.Things.Identity.BlazorClient.Store.UI.Actions;
using Rudai.Things.Identity.BlazorClient.Store.WeatherForecasts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Shared
{
    public partial class NavMenu
    {

        [Inject]
        public IDispatcher Dispatcher { get; set; }

        [Inject]
        private IState<UIState> UIState { get; set; }

        [Inject]
        private IState<CounterState> CounterState { get; set; }

        [Inject]
        private IState<ToursState> ToursState { get; set; }

        [Inject]
        private IState<BookingsState> BookingsState { get; set; }

        [Inject]
        private IState<WeatherForecastsState> WeatherForecastsState { get; set; }

        [Inject]
        IJSRuntime JS { get; set; }
        bool navSubMenuOpenState;

        [Parameter]
        public bool IsLeftDrawerOpen { get; set; }

        async Task ClickMe()
        {
            Console.WriteLine("test");
            await JS.InvokeAsync<object>("alert", "Successful OnClick!");
        }
        private bool collapseNavMenu = true;

        private string NavMenuCssClass => collapseNavMenu ? "collapse" : null;

        private void ToggleNavMenu()
        {
            collapseNavMenu = !collapseNavMenu;
        }

        private void ButtonClicked()
        {
            collapseNavMenu = !collapseNavMenu;
            ToggleLeftDrawer();
        }

        private void OpenLeftDrawer()
        {
            var action = new OpenLeftDrawerAction(true);
            Dispatcher.Dispatch(action);
            collapseNavMenu = !collapseNavMenu;
        }
        private void CloseLeftDrawer()
        {
            var action = new CloseLeftDrawerAction(false);
            Dispatcher.Dispatch(action);
            collapseNavMenu = !collapseNavMenu;
        }
        private void ToggleLeftDrawer()
        {
            var action = new ToggleLeftDrawerAction(UIState.Value.IsLeftDrawerOpen);
            Dispatcher.Dispatch(action);
            collapseNavMenu = !collapseNavMenu;
        }
    }
}
