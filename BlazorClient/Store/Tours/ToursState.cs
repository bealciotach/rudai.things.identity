﻿using Fluxor;
using Rudai.Things.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.Tours
{
    public class ToursState
    {
        public bool IsLoading { get; }
        public bool IsLoaded { get; }

#nullable enable
        public string? CurrentErrorMessage { get; }

#nullable disable
        public IEnumerable<Tour> Tours { get; }

        public Tour SelectedTour { get; }

#nullable enable
        public ToursState(bool isLoading, bool isLoaded, IEnumerable<Tour> tours, string? errorMessage)
        {
            IsLoading = isLoading;
            IsLoaded = isLoaded;
            Tours = tours ?? Array.Empty<Tour>();
            CurrentErrorMessage = errorMessage ?? string.Empty;
        }
        public ToursState(bool isLoading, bool isLoaded, IEnumerable<Tour> tours, Tour? selectedTour, string? errorMessage)
        {
            IsLoading = isLoading;
            IsLoaded = isLoaded;
            SelectedTour = selectedTour ?? null;
            Tours = tours ?? Array.Empty<Tour>();

            CurrentErrorMessage = errorMessage ?? string.Empty;
        }

#nullable disable
    }

}
