﻿using Rudai.Things.Models.Shared;

namespace Rudai.Things.Identity.BlazorClient.Store.Tours.Actions
{
    public class SelectTourAction
    {
        public SelectTourAction(Tour tour) => Tour = tour;

        public Tour Tour { get; }
    }
}
