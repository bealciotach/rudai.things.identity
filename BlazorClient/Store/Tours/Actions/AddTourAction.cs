﻿using Rudai.Things.Models.Shared;

namespace Rudai.Things.Identity.BlazorClient.Store.Tours.Actions
{
    public class AddTourAction
    {
        public AddTourAction(Tour tour) =>
            Tour = tour;

        public Tour Tour { get; }
    }
}
