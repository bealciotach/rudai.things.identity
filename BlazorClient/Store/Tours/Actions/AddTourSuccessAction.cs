﻿using Rudai.Things.Models.Shared;

namespace Rudai.Things.Identity.BlazorClient.Store.Tours.Actions
{
    public class AddTourSuccessAction
    {
        public Tour Tour { get; }
        public AddTourSuccessAction(Tour tour) =>
            Tour = tour;
    }
}
