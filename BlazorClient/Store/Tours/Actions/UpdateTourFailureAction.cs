﻿using Rudai.Things.Models.Shared.Actions;

namespace Rudai.Things.Identity.BlazorClient.Store.Tours.Actions
{
    public class UpdateTourFailureAction : FailureAction
    {
        public UpdateTourFailureAction(string message) 
            :base(message)
        { }
    }
}
