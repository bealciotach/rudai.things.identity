﻿using Rudai.Things.Models.Shared.Actions;

namespace Rudai.Things.Identity.BlazorClient.Store.Tours.Actions
{
    public class AddTourFailureAction: FailureAction
    {
        public AddTourFailureAction(string message) 
            :base(message)
        { }
    }
}
