﻿using Rudai.Things.Models.Shared;

namespace Rudai.Things.Identity.BlazorClient.Store.Tours.Actions
{
    public class UpdateTourSuccessAction
    {
        public Tour[] Tours { get; }
        public UpdateTourSuccessAction(Tour[] tours) =>
            Tours = tours;
    }
}
