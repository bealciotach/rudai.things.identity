﻿using Rudai.Things.Models.Shared.Actions;

namespace Rudai.Things.Identity.BlazorClient.Store.Tours.Actions
{
    public class GetToursFailureAction : FailureAction
    {
        public GetToursFailureAction(string message) 
            :base(message)
        { }
    }
}
