﻿using Rudai.Things.Models.Shared;
using System.Collections.Generic;

namespace Rudai.Things.Identity.BlazorClient.Store.Tours.Actions
{
    public class GetToursSuccessAction
    {
        public IEnumerable<Tour> Tours { get; }
        public GetToursSuccessAction(IEnumerable<Tour> tours)
        {
            Tours = tours;
        }
    }
}
