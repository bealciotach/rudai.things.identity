﻿using Rudai.Things.Models.Shared;

namespace Rudai.Things.Identity.BlazorClient.Store.Tours.Actions
{
    public class UpdateTourAction
    {
        public UpdateTourAction(Tour tour) =>
            Tour = tour;

        public Tour Tour { get; }
    }
}
