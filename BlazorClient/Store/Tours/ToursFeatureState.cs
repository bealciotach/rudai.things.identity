﻿using Fluxor;

namespace Rudai.Things.Identity.BlazorClient.Store.Tours
{
    public class ToursFeatureState : Feature<ToursState>
    {
        public override string GetName() => "Tours";
        protected override ToursState GetInitialState() =>
            new ToursState(isLoading: true, isLoaded: false, tours: null, errorMessage: null);
    }
}
