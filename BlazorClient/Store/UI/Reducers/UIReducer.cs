﻿using Fluxor;
using Rudai.Things.Identity.BlazorClient.Store.Counter.Actions;
using Rudai.Things.Identity.BlazorClient.Store.UI.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.UI.Reducers
{
    public class UIReducer
    {
        [ReducerMethod]
        public static UIState ReduceOpenLeftDrawerAction(UIState state, OpenLeftDrawerAction action) =>
        new UIState(isLeftDrawerOpen: true, state.CurrentTheme);

        [ReducerMethod]
        public static UIState ReduceCloseLeftDrawerAction(UIState state, CloseLeftDrawerAction action) =>
            new UIState(isLeftDrawerOpen: false, state.CurrentTheme);

        [ReducerMethod]
        public static UIState ReduceToggleLeftDrawerAction(UIState state, ToggleLeftDrawerAction action) =>
            new UIState(isLeftDrawerOpen: !state.IsLeftDrawerOpen, theme: state.CurrentTheme);

        [ReducerMethod]
        public static UIState ReduceSwitchThemeAction(UIState state, SwitchThemeAction action) =>
    new UIState(theme: action.Theme);

    }
}
