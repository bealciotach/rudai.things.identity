﻿using Fluxor;
using MatBlazor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.UI
{
    public class UIState
    {
        public List<MatTheme> Themes = new List<MatTheme>()
        {
            new MatTheme() {
                Background = "#fff", // The theme background color
                OnPrimary = "#fff", //  Text color on top of a primary background
                Primary = "#003d6a",//MatThemeColors.Teal._500.Value,
                Secondary = "#868e96", //MatThemeColors.BlueGrey._500.Value,
                OnSurface =  MatThemeColors.Blue._500.Value, // Text color on top of a surface background
                Surface = MatThemeColors.Teal._500.Value, // The theme surface color
            },
            new MatTheme() {
                Primary = "003d6a", // MatThemeColors.Blue._500.Value,
                Secondary = "#868e96", // MatThemeColors.LightGreen._500.Value
            }
        };

        public bool IsLeftDrawerOpen { get; set; }
        public MatTheme CurrentTheme { get; set; }

#nullable enable
        public UIState(bool isLeftDrawerOpen, MatTheme? theme)
        {
            IsLeftDrawerOpen = isLeftDrawerOpen;
            CurrentTheme = theme ?? Themes[0];
        }


        public UIState(MatTheme? theme)
        {
            CurrentTheme = theme ?? Themes[1];
        }
        
 #nullable disable
    }

    public class UIFeatureState : Feature<UIState>
    {
        public override string GetName() => "UI State";
        protected override UIState GetInitialState() =>
            new UIState(isLeftDrawerOpen: true, null);
    }
}
