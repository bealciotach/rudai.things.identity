﻿using MatBlazor;

namespace Rudai.Things.Identity.BlazorClient.Store.UI.Actions
{
    public class SwitchThemeAction
    {
        public MatTheme Theme;

        public SwitchThemeAction(MatTheme theme)
        {
            Theme = theme;
        }
    }
}