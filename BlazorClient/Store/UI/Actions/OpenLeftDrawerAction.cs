﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.UI.Actions
{
    public class OpenLeftDrawerAction
    {
        public OpenLeftDrawerAction(bool isLeftFrawerOpen) => IsOpen = isLeftFrawerOpen;

        public bool IsOpen { get; }
    }
}
