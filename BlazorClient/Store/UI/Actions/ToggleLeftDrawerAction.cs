﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.UI.Actions
{
    public class ToggleLeftDrawerAction
    {
        public ToggleLeftDrawerAction(bool isLeftDrawerOpen) => IsOpen = isLeftDrawerOpen;

        public bool IsOpen { get; }
    }
}
