﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.UI.Actions
{
    public class CloseLeftDrawerAction
    {
        public CloseLeftDrawerAction(bool isLeftFrawerOpen) => IsOpen = isLeftFrawerOpen;

        public bool IsOpen { get; }
    }
}
