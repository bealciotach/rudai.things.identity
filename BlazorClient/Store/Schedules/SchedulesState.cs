﻿using Rudai.Things.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.Schedules
{
    public class SchedulesState
    {
        public bool IsLoading { get; }
        public bool IsLoaded { get; }
#nullable enable
        public string? CurrentErrorMessage { get; }
#nullable disable
        public IEnumerable<Schedule> Schedules { get; }

        public Schedule SelectedSchedule { get; }

        public SchedulesState(bool isLoading, bool isLoaded, IEnumerable<Schedule> schedules, Schedule? selectedSchedule, string? errorMessage)
        {
            IsLoading = isLoading;
            IsLoaded = isLoaded;
            SelectedSchedule = selectedSchedule ?? null;
            Schedules = schedules ?? Array.Empty<Schedule>();

            CurrentErrorMessage = errorMessage ?? string.Empty;
        }
    }
}
