﻿using Fluxor;
using Rudai.Things.Identity.BlazorClient.Services;
using Rudai.Things.Identity.BlazorClient.Store.Bookings.Actions;
using Rudai.Things.Identity.BlazorClient.Store.Schedules.Actions;
using System;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.Schedules.Effects
{
    public class SchedulesActionEffects
	{
		private readonly ISchedulesService SchedulesService;
		public SchedulesActionEffects(ISchedulesService schedulesService)
		{
			SchedulesService = schedulesService;
		}
		[EffectMethod]
		public async Task HandleGetSchedulesAction(GetSchedulesAction action, IDispatcher dispatcher)
		{
            try
            {
				var schedules = await SchedulesService.GetItemsAsync();
				dispatcher.Dispatch(new GetSchedulesSuccessAction(schedules));
			}
            catch (Exception ex)
            {

				dispatcher.Dispatch(new GetSchedulesFailureAction(ex.Message));
			}
		}
		//[EffectMethod]
		//public async Task HandleAddBookingAction(AddScheduleAction action, IDispatcher dispatcher)
		//{
		//	try
		//	{
		//		var bookings = await SchedulesService.AddItemAsync(action.Booking);
		//		dispatcher.Dispatch(new AddBookingSuccessAction(action.Booking));
		//	}
		//	catch (Exception ex)
		//	{

		//		dispatcher.Dispatch(new AddBookingFailureAction(ex.Message));
		//	}
		//}
		//[EffectMethod]
		//public async Task HandleUpdateBookingAction(UpdateBookingAction action, IDispatcher dispatcher)
		//{
		//	try
		//	{
		//		var bookings = await BookingsService.UpdateBookingAsync(action.Booking);
		//		dispatcher.Dispatch(new UpdateBookingSuccessAction(bookings));
		//	}
		//	catch (Exception ex)
		//	{

		//		dispatcher.Dispatch(new UpdateBookingFailureAction(ex.Message));
		//	}
		//}
	}
}
