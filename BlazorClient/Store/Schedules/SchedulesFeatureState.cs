﻿using Fluxor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.Schedules
{
    public class SchedulesFeatureState : Feature<SchedulesState>
    {
        public override string GetName() => "Schedules";
        protected override SchedulesState GetInitialState() =>
            new SchedulesState(isLoading: true, isLoaded: false, schedules: null, selectedSchedule: null, errorMessage: null);

    }
}
