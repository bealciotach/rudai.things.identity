﻿using Rudai.Things.Models.Shared.Actions;

namespace Rudai.Things.Identity.BlazorClient.Store.Schedules.Actions
{
    public class GetSchedulesFailureAction : FailureAction
    {
        public GetSchedulesFailureAction(string message) 
            :base(message)
        { }
    }
}
