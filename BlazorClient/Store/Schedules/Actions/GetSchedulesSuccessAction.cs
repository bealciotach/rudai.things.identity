﻿using Rudai.Things.Models.Shared;
using System.Collections.Generic;

namespace Rudai.Things.Identity.BlazorClient.Store.Schedules.Actions
{
    public class GetSchedulesSuccessAction
    {
        public IEnumerable<Schedule> Schedules { get; }
        public GetSchedulesSuccessAction(IEnumerable<Schedule> schedules)
        {
            Schedules = schedules;
        }
    }
}
