﻿using Fluxor;
using Rudai.Things.Identity.BlazorClient.Store.Counter.Actions;
using Rudai.Things.Identity.BlazorClient.Store.Tours.Actions;
using Rudai.Things.Identity.BlazorClient.Store.WeatherForecasts;
using Rudai.Things.Identity.BlazorClient.Store.WeatherForecasts.Actions;
using Rudai.Things.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.WeatherForecasts.Reducers
{
    public class WeatherForecastsReducer
    {
        [ReducerMethod]
        public static WeatherForecastsState ReduceGetWeatherForcastsAction(WeatherForecastsState state, GetWeatherForcastsAction action) =>
        new WeatherForecastsState(isLoading: true, forecasts: null);

        [ReducerMethod]
        public static WeatherForecastsState ReduceGetToursActionSuccess(WeatherForecastsState state, GetWeatherForcastsSuccessAction action) => 
            new WeatherForecastsState(isLoading: false, forecasts: action.Forecasts);

    }
}
