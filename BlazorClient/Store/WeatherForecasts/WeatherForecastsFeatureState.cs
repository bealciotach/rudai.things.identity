﻿using Fluxor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.WeatherForecasts
{
    public class WeatherForecastsFeatureState : Feature<WeatherForecastsState>
    {
        public override string GetName() => "Weather Forcasts";
        protected override WeatherForecastsState GetInitialState() =>
            new WeatherForecastsState(isLoading: true, forecasts: null);
    }
}
