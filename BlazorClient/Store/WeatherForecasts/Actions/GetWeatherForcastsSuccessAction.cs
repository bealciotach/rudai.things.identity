﻿using Rudai.Things.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.WeatherForecasts.Actions
{
    public class GetWeatherForcastsSuccessAction
    {
        public IEnumerable<WeatherForecast> Forecasts { get; }
        public GetWeatherForcastsSuccessAction(IEnumerable<WeatherForecast> forecasts)
        {
            Forecasts = forecasts;
        }
    }
}
