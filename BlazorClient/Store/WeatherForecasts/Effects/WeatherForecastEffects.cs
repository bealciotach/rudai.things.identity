﻿using Fluxor;
using Rudai.Things.Identity.BlazorClient.Services;
using Rudai.Things.Identity.BlazorClient.Store.WeatherForecasts.Actions;
using System;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.WeatherForecasts.Effects
{
    public class WeatherForecastEffects
	{
		private readonly IWeatherForecastService WeatherForecastService;


		public WeatherForecastEffects(IWeatherForecastService weatherForecastService)
		{
			WeatherForecastService = weatherForecastService;
		}
		[EffectMethod]
		public async Task HandleGetWeatherForcastsAction(GetWeatherForcastsAction action, IDispatcher dispatcher)
		{
			var forecasts = await WeatherForecastService.GetForecastAsync(DateTime.Now);
			dispatcher.Dispatch(new GetWeatherForcastsSuccessAction(forecasts));
		}
	}
}
