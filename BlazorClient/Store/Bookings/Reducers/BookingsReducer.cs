﻿using Fluxor;
using Rudai.Things.Identity.BlazorClient.Store.Bookings.Actions;
using Rudai.Things.Models.Shared;
using System.Collections.Generic;
using System.Linq;

namespace Rudai.Things.Identity.BlazorClient.Store.Bookings.Reducers
{
    public class BookingsReducer
    {

        [ReducerMethod]
        public static BookingsState ReduceGetBookingsAction(BookingsState state, GetBookingsAction action) =>
        new BookingsState(isLoading: true, false, bookings: null, errorMessage: null);

        [ReducerMethod]
        public static BookingsState ReduceGetBookingsActionSuccess(BookingsState state, GetBookingsSuccessAction action) => 
            new BookingsState(isLoading: false, true, bookings: action.Bookings, null);

        [ReducerMethod]
        public static BookingsState ReduceGetBookingsFailureAction(BookingsState state, GetBookingsFailureAction action)
        {
            return new BookingsState(false, true, null, null, action.ErrorMessage);
        }

        [ReducerMethod]
        public static BookingsState ReduceAddBookingAction(BookingsState state, AddBookingFailureAction action) =>
            new BookingsState(isLoading: true, isLoaded: false, bookings: null, null);

        [ReducerMethod]
        public static BookingsState ReduceAddBookingSuccessAction(BookingsState state, AddBookingSuccessAction action)
        {
            // Grab a reference to the current todo list, or initialize one if we do not currently have any loaded
            var output = state.Bookings is null ?
                new List<Booking>() :
                state.Bookings.ToList();

            // Add the newly created todo to our list and sort by ID
            output.Add(action.Booking);
            output = output
                .OrderBy(t => t.Id)
                .ToList();

            return new BookingsState(false, true, output, state.SelectedBooking, null);
        }


        [ReducerMethod]
        public static BookingsState ReduceAddBookingFailureAction(BookingsState state, AddBookingFailureAction action)
        {
            var output = state.Bookings is null ?
                new List<Booking>() :
                state.Bookings.ToList();

            output = output
                .OrderBy(t => t.Id)
                .ToList();

            return new BookingsState(false, true, output, state.SelectedBooking, action.ErrorMessage);
        }

        [ReducerMethod]
        public static BookingsState ReduceUpdateBookingAction(BookingsState state, UpdateBookingAction action) => 
            new BookingsState(isLoading: true, isLoaded: false, bookings: state.Bookings, null);

        [ReducerMethod]
        public static BookingsState ReduceUpdateBookingSuccessAction(BookingsState state, UpdateBookingSuccessAction action)
        {
            var output = action.Bookings is null ?
                new List<Booking>() :
                action.Bookings.ToList();

            output = output
                .OrderBy(t => t.Id)
                .ToList();

            return new BookingsState(false, true, output, null);
        }

        [ReducerMethod]
        public static BookingsState ReduceUpdateBookingFailureAction(BookingsState state, UpdateBookingFailureAction action)
        {
            var tours = state.Bookings is null ?
                new List<Booking>() :
                state.Bookings.ToList();

            tours = tours
                .OrderBy(t => t.Id)
                .ToList();

            return new BookingsState(false, true, tours, state.SelectedBooking, action.ErrorMessage);
        }

        [ReducerMethod]
        public static BookingsState ReduceSelectBookingAction(BookingsState state, SelectBookingAction action)
        {
            var output = state.Bookings is null ?
                new List<Booking>() :
                state.Bookings.ToList();

            output = output
                .OrderBy(t => t.Id)
                .ToList();

            return new BookingsState(false, state.IsLoaded, output, action.Booking, null);
        }

    }
}
