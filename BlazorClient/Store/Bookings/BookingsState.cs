﻿using Rudai.Things.Models.Shared;
using System;
using System.Collections.Generic;

namespace Rudai.Things.Identity.BlazorClient.Store.Bookings
{
    public class BookingsState
    {
        public bool IsLoading { get; }
        public bool IsLoaded { get; }
#nullable enable
        public string? CurrentErrorMessage { get; }
#nullable disable
        public IEnumerable<Booking> Bookings { get; }

        public Tour SelectedTour { get; }
        public Booking SelectedBooking { get; }

#nullable enable
        public BookingsState(bool isLoading, bool isLoaded, IEnumerable<Booking> bookings, string? errorMessage)
        {
            IsLoading = isLoading;
            IsLoaded = isLoaded;
            Bookings = bookings ?? Array.Empty<Booking>();
            CurrentErrorMessage = errorMessage ?? string.Empty;
        }
        public BookingsState(bool isLoading, bool isLoaded, IEnumerable<Booking> bookings, Booking? selectedBooking, string? errorMessage)
        {
            IsLoading = isLoading;
            IsLoaded = isLoaded;
            SelectedBooking = selectedBooking ?? null;
            Bookings = bookings ?? Array.Empty<Booking>();

            CurrentErrorMessage = errorMessage ?? string.Empty;
        }

#nullable disable
    }

}
