﻿using Rudai.Things.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.Bookings.Actions
{
    public class SelectBookingAction
    {
        public SelectBookingAction(Booking booking) => Booking = booking;

        public Booking Booking { get; }
    }
}
