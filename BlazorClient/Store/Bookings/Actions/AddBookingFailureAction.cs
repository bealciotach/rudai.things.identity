﻿
using Rudai.Things.Models.Shared.Actions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.Bookings.Actions
{
    public class AddBookingFailureAction : FailureAction
    {
        public AddBookingFailureAction(string message) 
            :base(message)
        { }
    }
}
