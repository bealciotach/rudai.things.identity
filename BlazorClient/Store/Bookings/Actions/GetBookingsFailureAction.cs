﻿using Rudai.Things.Models.Shared.Actions;

namespace Rudai.Things.Identity.BlazorClient.Store.Bookings.Actions
{
    public class GetBookingsFailureAction : FailureAction
    {
        public GetBookingsFailureAction(string message) 
            :base(message)
        { }
    }
}
