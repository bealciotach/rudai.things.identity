﻿using Rudai.Things.Models.Shared;

namespace Rudai.Things.Identity.BlazorClient.Store.Bookings.Actions
{
    public class AddBookingSuccessAction
    {
        public Booking Booking { get; }
        public AddBookingSuccessAction(Booking booking) =>
            Booking = booking;
    }
}
