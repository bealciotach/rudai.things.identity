﻿using Rudai.Things.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.Bookings.Actions
{
    public class GetBookingsSuccessAction
    {
        public IEnumerable<Booking> Bookings { get; }
        public GetBookingsSuccessAction(IEnumerable<Booking> bookings)
        {
            Bookings = bookings;
        }
    }
}
