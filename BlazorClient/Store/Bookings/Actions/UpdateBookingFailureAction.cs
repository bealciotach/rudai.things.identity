﻿using Rudai.Things.Models.Shared.Actions;

namespace Rudai.Things.Identity.BlazorClient.Store.Bookings.Actions
{
    public class UpdateBookingFailureAction : FailureAction
    {
        public UpdateBookingFailureAction(string message) 
            :base(message)
        { }
    }
}
