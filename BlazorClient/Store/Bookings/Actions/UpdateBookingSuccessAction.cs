﻿using Rudai.Things.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.Bookings.Actions
{
    public class UpdateBookingSuccessAction
    {
        public Booking[] Bookings { get; }
        public UpdateBookingSuccessAction(Booking[] bookings) =>
            Bookings = bookings;
    }
}
