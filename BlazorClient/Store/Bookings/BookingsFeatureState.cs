﻿using Fluxor;

namespace Rudai.Things.Identity.BlazorClient.Store.Bookings
{
    public class BookingsFeatureState : Feature<BookingsState>
    {
        public override string GetName() => "Bookings";
        protected override BookingsState GetInitialState() =>
            new BookingsState(isLoading: true, isLoaded: false, bookings: null, errorMessage: null);
    }
}
