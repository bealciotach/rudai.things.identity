﻿using Fluxor;
using Rudai.Things.Identity.BlazorClient.Store.Counter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.Tours
{
    public class CounterFeatureState : Feature<CounterState>
    {
        public override string GetName() => "Counter";
        protected override CounterState GetInitialState() =>
            new CounterState(count: 0);
    }
}
