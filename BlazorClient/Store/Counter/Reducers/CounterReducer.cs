﻿using Fluxor;
using Rudai.Things.Identity.BlazorClient.Store.Counter.Actions;

namespace Rudai.Things.Identity.BlazorClient.Store.Counter.Reducers
{
    public class CounterReducer
    {
        [ReducerMethod]
        public static CounterState ReduceDecrementCounterAction(CounterState state, DecrementCounterAction action) =>
        new CounterState(count: state.Count > 0 ? state.Count - 1 : state.Count);

        [ReducerMethod]
        public static CounterState ReduceIncrementCounterAction(CounterState state, IncrementCounterAction action) => 
            new CounterState(count: state.Count + 1);

    }
}
