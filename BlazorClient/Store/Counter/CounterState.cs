﻿using Fluxor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Identity.BlazorClient.Store.Counter
{
    public class CounterState
    {
        public int Count { get; }
        public CounterState(int count)
        {
            Count = count;

        }
    }
}
