﻿using Fluxor;
using Microsoft.AspNetCore.Components;
using Rudai.Things.Identity.BlazorClient.Services;
using Rudai.Things.Identity.BlazorClient.Store.Tours;
using Rudai.Things.Models.Shared;
using System;

namespace Rudai.Things.Identity.BlazorClient.Pages
{
    public partial class Tours
    {
        private string _currentSelectedTask;

        [Inject]
        private IState<ToursState> ToursState { get; set; }

        [Inject]
        private StateFacade StateFacade { get; set; }
        [Inject]
        private NavigationManager NavigationManager { get; set; }

        protected override void OnInitialized()
        {
            base.OnInitialized();
            StateFacade.LoadTours();
        }

        private void AddTour()
        {
            Tour tour = new Tour(Guid.NewGuid(), string.Empty, string.Empty, DateTime.Now.AddDays(4));
            if (tour != null)
            {
                StateFacade.SelectTour(tour);
                // Route to detail
                NavigationManager.NavigateTo(string.Format("{0}/{1}/{2}", "/tourdetailform", "Add", tour.Id));
            }
        }

        private void SelectTour(Tour tour)
        {
            if (tour != null)
            {
                StateFacade.SelectTour(tour);
                // Route to detail
                NavigationManager.NavigateTo(string.Format("{0}/{1}/{2}", "/tourdetailform", "Update", tour.Id));
            }
        }

        public void SelectionChangedEvent(object row)
        {
            if (row == null)
            {
                _currentSelectedTask = "empty";
            }
            else
            {
                var tour = (Tour)row;
                _currentSelectedTask = string.Format("Tour Nr. {0} has been selected", tour.Id);
                SelectTour(tour);
            }

            this.StateHasChanged();
        }

    }
}
