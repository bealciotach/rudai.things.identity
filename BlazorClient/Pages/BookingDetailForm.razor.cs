﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fluxor;
using Microsoft.AspNetCore.Components;
using Rudai.Things.Identity.BlazorClient.Services;
using Rudai.Things.Identity.BlazorClient.Store.Bookings;
using Rudai.Things.Models.Shared;

namespace Rudai.Things.Identity.BlazorClient.Pages
{
    public partial class BookingDetailForm
    {
        [Inject]
        private NavigationManager NavigationManager { get; set; }

        /// <summary>
        /// The TourState
        /// </summary>
        [Inject]
        private IState<BookingsState> BookingState { get; set; }

        /// <summary>
        /// The StateFacade
        /// </summary>
        [Inject]
        private StateFacade StateFacade { get; set; }

        private BookingValidationModel validationModel = new BookingValidationModel();

        #nullable enable
        /// <summary>
        /// TourId
        /// </summary>
        [Parameter]
        public string? BookingId { get; set; }
        
        #nullable disable

        [Parameter]
        public string Mode { get; set; }


        protected override void OnInitialized()
        {
            //try
            //{
            //    validationModel.Id = BookingState.Value.SelectedBooking.Id;
            //    validationModel.Description = BookingState.Value.SelectedBooking.Description;
            //    validationModel.Title = BookingState.Value.SelectedBooking.Title;
            //    validationModel.BookingDate = BookingState.Value.SelectedBooking.BookingDate;
            //}
            //catch (Exception ex)
            //{
            //    Console.Write(ex.Message);
            //}

            validationModel.BookingDate = BookingState.Value.SelectedBooking.BookingDate;
            validationModel.Description = BookingState.Value.SelectedBooking.Description;
            validationModel.Id = BookingState.Value.SelectedBooking.Id;
            validationModel.Title = BookingState.Value.SelectedBooking.Title;

            base.OnInitialized();
        }

        public void HandleValidSubmit()
        {
            // We use the bang operator (!) to tell the compiler we'll know this string field will not be null
            if (Mode == "Add")
            {
                StateFacade.AddBooking(validationModel.Id, validationModel.Title, validationModel.Description, validationModel.BookingDate);
                if (BookingState.Value.IsLoaded)
                {
                    this.StateHasChanged();
                    Navigate();
                }
            }
            else if (Mode == "Update")
            {
                StateFacade.UpdateBooking(validationModel.Id, validationModel.Title, validationModel.Description, validationModel.BookingDate);
                if (!BookingState.Value.IsLoaded)
                {
                    this.StateHasChanged();
                    Navigate();
                }
            }
        }

        private void Navigate()
        {
            NavigationManager.NavigateTo(string.Format("{0}", "/tours"), false);

        }
    }
}