﻿using Fluxor;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Rudai.Things.Identity.BlazorClient.Services;
using Rudai.Things.Identity.BlazorClient.Store.WeatherForecasts;

namespace Rudai.Things.Identity.BlazorClient.Pages
{
    [Authorize]
    public partial class FetchData
    {

        [Inject]
        private StateFacade StateFacade { get; set; }
        [Inject]
        private IState<WeatherForecastsState> WeatherState { get; set; }

        protected override void OnInitialized()
        {
            base.OnInitialized();
            StateFacade.LoadWeatherForecast();
        }
    }
}
