﻿using Fluxor;
using Microsoft.AspNetCore.Components;
using Rudai.Things.Identity.BlazorClient.Services;
using Rudai.Things.Identity.BlazorClient.Store.Tours;
using Rudai.Things.Identity.BlazorClient;
using System;
using Rudai.Things.Models.Shared;

namespace Rudai.Things.Identity.BlazorClient.Pages
{
    public enum FormMode
    {
        Add,
        Update
    }
    public partial class TourDetailForm
    {
        [Inject]
        private NavigationManager NavigationManager { get; set; }

        /// <summary>
        /// The TourState
        /// </summary>
        [Inject]
        private IState<ToursState> TourState { get; set; }

        /// <summary>
        /// The StateFacade
        /// </summary>
        [Inject]
        private StateFacade StateFacade { get; set; }

        private TourValidationModel validationModel = new TourValidationModel();

#nullable enable
        /// <summary>
        /// TourId
        /// </summary>
        [Parameter]
        public string? TourId { get; set; }

#nullable disable

        [Parameter]
        public string Mode { get; set; }

        [Parameter]
        public FormMode FormMode { get; set; }

        //protected override void OnAfterRender(bool firstRender)
        //{
        //    if (firstRender)
        //    {
        //        TourState.StateChanged += 
        //    }
        //    base.OnAfterRender(firstRender);
        //}

        protected override void OnInitialized()
        {
            try
            {
                validationModel.Description = TourState.Value.SelectedTour.Description;
                validationModel.Id = TourState.Value.SelectedTour.Id;
                validationModel.Title = TourState.Value.SelectedTour.Title;
                validationModel.TourDate = TourState.Value.SelectedTour.TourDate;
            }
            catch (Exception ex)
            {

                //throw;
            }

            base.OnInitialized();
        }

        private void HandleValidSubmit()
        {
            // We use the bang operator (!) to tell the compiler we'll know this string field will not be null
            if (Mode == "Add")
            {
                StateFacade.AddTour(validationModel.Id, validationModel.Title, validationModel.Description, validationModel.TourDate);
                if (TourState.Value.IsLoaded)
                {
                    Navigate();
                }
            }
            else
            {
                StateFacade.UpdateTour(validationModel.Id, validationModel.Title, validationModel.Description, validationModel.TourDate);
                if (!TourState.Value.IsLoaded)
                {
                    Navigate();
                }
            }
        }

        private void Navigate()
        {
            NavigationManager.NavigateTo(string.Format("{0}", "/tours"), false);

        }

    }
}
