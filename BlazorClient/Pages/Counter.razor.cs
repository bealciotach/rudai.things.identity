﻿using Fluxor;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Rudai.Things.Identity.BlazorClient.Store.Counter;
using Rudai.Things.Identity.BlazorClient.Store.Counter.Actions;

namespace Rudai.Things.Identity.BlazorClient.Pages
{
    public partial class Counter
    {
        [Inject]
        private IState<CounterState> CounterState { get; set; }
        [Inject]
        public IDispatcher Dispatcher { get; set; }

        private void IncrementCount(MouseEventArgs e)
        {
            var action = new IncrementCounterAction();
            Dispatcher.Dispatch(action);
        }
        private void DecrementCount(Microsoft.AspNetCore.Components.Web.MouseEventArgs e)
        {
            var action = new DecrementCounterAction();
            Dispatcher.Dispatch(action);
        }
    }
}
