﻿using Fluxor;
using Microsoft.AspNetCore.Components;
using Rudai.Things.Identity.BlazorClient.Services;
using Rudai.Things.Identity.BlazorClient.Store.Bookings;
using Rudai.Things.Identity.BlazorClient.Store.Schedules;
using Rudai.Things.Models.Shared;
using System;


namespace Rudai.Things.Identity.BlazorClient.Pages
{
    public partial class Schedules
    {
        private string _currentSelectedTask;
        [Inject]
        private IState<SchedulesState> SchedulesState { get; set; }

        [Inject]
        private StateFacade StateFacade { get; set; }
        [Inject]
        private NavigationManager NavigationManager { get; set; }

        protected override void OnInitialized()
        {
            base.OnInitialized();
            StateFacade.LoadSchedules();
        }

        private void AddSchedule()
        {
            Schedule item = new Schedule()
            {
                Id = Guid.NewGuid(),
                ByDay = "Monday"
            };
            if (item != null)
            {
                StateFacade.SelectSchedule(item);
                NavigationManager.NavigateTo(string.Format("{0}/{1}/{2}", "/bookingdetailform", "Add", item.Id));
            }
        }

        private void SelectSchedule(Schedule item)
        {
            if (item != null)
            {
                StateFacade.SelectSchedule(item);
                // Route to detail
                NavigationManager.NavigateTo(string.Format("{0}/{1}/{2}", "/scheduledetailform", "Update", item.Id));
            }
        }

        public void SelectionChangedEvent(object row)
        {
            if (row == null)
            {
                _currentSelectedTask = "empty";
            }
            else
            {
                var item = (Schedule)row;
                _currentSelectedTask = string.Format("Schedule Nr. {0} has been selected", item.Id);
                SelectSchedule(item);
            }

            this.StateHasChanged();
        }
    }
}
