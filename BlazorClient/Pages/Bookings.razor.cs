﻿using Fluxor;
using Microsoft.AspNetCore.Components;
using Rudai.Things.Identity.BlazorClient.Services;
using Rudai.Things.Identity.BlazorClient.Store.Bookings;
using Rudai.Things.Models.Shared;
using System;

namespace Rudai.Things.Identity.BlazorClient.Pages
{
    public partial class Bookings
    {
        private string _currentSelectedTask;
        [Inject]
        private IState<BookingsState> BookingsState { get; set; }

        [Inject]
        private StateFacade StateFacade { get; set; }
        [Inject]
        private NavigationManager NavigationManager { get; set; }

        protected override void OnInitialized()
        {
            base.OnInitialized();
            StateFacade.LoadBookings();
        }

        private void AddBooking()
        {
            Booking booking = new Booking(Guid.NewGuid(), string.Empty, string.Empty, DateTime.Now.AddDays(4));
            if (booking != null)
            {
                StateFacade.SelectBooking(booking);
                NavigationManager.NavigateTo(string.Format("{0}/{1}/{2}", "/bookingdetailform", "Add", booking.Id));
            }
        }

        private void SelectBooking(Booking booking)
        {
            if (booking != null)
            {
                StateFacade.SelectBooking(booking);
                // Route to detail
                NavigationManager.NavigateTo(string.Format("{0}/{1}/{2}", "/bookingdetailform", "Update", booking.Id));
            }
        }

        public void SelectionChangedEvent(object row)
        {
            if (row == null)
            {
                _currentSelectedTask = "empty";
            }
            else
            {
                var booking = (Booking)row;
                _currentSelectedTask = string.Format("Booking Nr. {0} has been selected", booking.Id);
                SelectBooking(booking);
            }

            this.StateHasChanged();
        }
    }
}
