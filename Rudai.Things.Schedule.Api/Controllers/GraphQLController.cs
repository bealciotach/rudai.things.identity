﻿using GraphQL;
using GraphQL.NewtonsoftJson;
using GraphQL.SystemTextJson;
using GraphQL.Types;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Rudai.Things.Schedules.Api.Models.Graph;
//using Rudai.Things.Tours.Api.Wrappers;
using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Rudai.Things.Schedules.Api.Controllers
{
    //[Authorize]
    [Route("[controller]")] 
    public class GraphQLController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IDocumentExecuter _documentExecuter;
        private readonly IDocumentWriter _writer;
        private readonly ISchema _schema;

        public GraphQLController(
            IHttpContextAccessor httpContextAccessor, 
            IDocumentExecuter documentExecuter, 
            IDocumentWriter writer)
        {
            _httpContextAccessor = httpContextAccessor;
            //_schema = schema;
            _documentExecuter = documentExecuter;
            _writer = writer;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] GraphQLQuery query, [FromServices] ISchema schema)
        {
            if (query == null) { throw new ArgumentNullException(nameof(query)); }

            var inputs = query.Variables.ToInputs();
            StringBuilder sb = new StringBuilder();

            foreach (var item in inputs.Keys)
            {
                sb.Append(item);
                sb.Append(",");
            }

            Console.WriteLine(string.Format("schema: {0} Query : {1} inputs: {2}", schema.Description, query.ToString(), sb.ToString()));
            var executionOptions = new ExecutionOptions
            {
                Schema = schema,
                Query = query.Query,
                Inputs = inputs
                //ValidationRules = new List<IValidationRule> { _validationRule },
                //UserContext = _httpContextAccessor.HttpContext?.AsDictionary()
            };
            //throw new Exception("BLA");
            var result = await _documentExecuter.ExecuteAsync(executionOptions).ConfigureAwait(false);

            if (result.Errors?.Count > 0)
            {
                return BadRequest(result);
            }
            //var pagedResult = new PagedResponse<object>(result, 10, 100);

            //return Ok(pagedResult);



            var json = await _writer.WriteToStringAsync(result);

            var httpResult = result.Errors?.Count > 0
                ? HttpStatusCode.BadRequest
                : HttpStatusCode.OK;
            Console.WriteLine(string.Format("httpResult: {0} json : {1}", httpResult, json));

            return Content(json, "application/json");
        }
    }
}
