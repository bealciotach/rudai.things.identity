﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Rudai.Things.Models.Shared;
using Rudai.Things.Schedules.Api.Data;
using Rudai.Things.Schedules.Api.Models;
using Rudai.Things.Schedules.Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rudai.Things.Schedules.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SchedulesController : ControllerBase
    {
        private readonly SchedulesContext _context;
        private readonly ScheduleRepository _repo;

        public SchedulesController(SchedulesContext context, ScheduleRepository repo)
        {
            _context = context;
            _repo = repo;
        }

        // GET: api/Schedules
        [HttpGet]
        public async Task<IEnumerable<Schedule>> Get()
        {
            IEnumerable<Schedule> items = await _repo.GetAll();
            List<Schedule> output = new List<Schedule>();
            foreach (var item in items)
            {
               if (!string.IsNullOrEmpty(item.ByDay))
                {
                    item.ByDay = GetDaysOfWeek(item.ByDay);
                }
                output.Add(item);
            }
            return output;
        }

        // GET: api/Schedules/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Schedule>> Get(Guid id)
        {
            var tour = await _context.Schedules.FindAsync(id);
            //var tour = await _repo.GetById(id);

            if (tour == null)
            {
                return NotFound();
            }

            return tour;
        }

        // PUT: api/Schedules/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSchedule(Guid id, Schedule schedule)
        {
            if (id != schedule.Id)
            {
                return BadRequest();
            }

            _context.Entry(schedule).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Exists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Schedules
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Schedule>> PostSchedule(Schedule schedule)
        {
            _context.Set<Schedule>().Add(schedule);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Get", new { id = schedule.Id }, schedule);
        }

        // DELETE: api/Schedules/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Schedule>> DeleteSchedule(Guid id)
        {
            var tour = await _context.Schedules.FindAsync(id);
            if (tour == null)
            {
                return NotFound();
            }

            _context.Schedules.Remove(tour);
            await _context.SaveChangesAsync();

            return tour;
        }

        private bool Exists(Guid id)
        {
            return _context.Schedules.Any(e => e.Id == id);
        }

        private string GetDaysOfWeek(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            string output = string.Empty;
            StringBuilder sb = new StringBuilder();
            var days = input.Split(",");
            for (int i = 0; i < days.Count(); i++)
            {
                bool valid = false;
                int validValue = 0;
                valid = int.TryParse(days[i], out validValue);

                if (valid)
                {
                    string day = Enum.GetName(typeof(Schema.NET.DayOfWeek), validValue);
                    sb.Append(day);

                    if (i < days.Count() - 1)
                    {
                        sb.Append(",");
                    }                
                }
            }

            output = sb.ToString();
            sb = null;

            return output;
        }
    }
}
