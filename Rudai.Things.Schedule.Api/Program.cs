using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Rudai.Things.Schedules.Api.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Schedules.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //var seed = args.Contains("/seed");
            //if (seed)
            //{
            //    args = args.Except(new[] { "/seed" }).ToArray();
            //}

            //var host = CreateHostBuilder(args).Build();

            //if (seed)
            //{
            //    var config = host.Services.GetRequiredService<IConfiguration>();
            //    var connectionString = config.GetConnectionString("DefaultConnection");
            //    SeedData.EnsureSeedData(connectionString);
            //}

            //host.Run();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
