﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Rudai.Things.Schedules.Api.Data;
using Rudai.Things.Schedules.Api.Services;

namespace Rudai.Things.Schedules.Api.Helpers
{
    // https://github.com/graphql-dotnet/graphql-dotnet/issues/648#issuecomment-431489339
    public class ContextServiceLocator
    {
        public SchedulesContext SchedulesContext => _httpContextAccessor.HttpContext.RequestServices.GetRequiredService<SchedulesContext>();
        public ScheduleRepository ScheduleRepository => _httpContextAccessor.HttpContext.RequestServices.GetRequiredService<ScheduleRepository>();

        private readonly IHttpContextAccessor _httpContextAccessor;

        public ContextServiceLocator(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
    }
}
