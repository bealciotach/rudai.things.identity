﻿using Microsoft.EntityFrameworkCore;
using Rudai.Things.Schedules.Api.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Rudai.Things.Schedules.Api.Core
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected readonly SchedulesContext _context;
        public BaseRepository(SchedulesContext context)
        {
            _context = context;
        }
        public void Add(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public void AddRange(IEnumerable<T> entities)
        {
            _context.Set<T>().AddRange(entities);
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().Where(expression);
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await _context.Set<T>().ToListAsync();
        }

        public T GetById(Guid id)
        {
            return _context.Set<T>().Find(id);
        }

        public void Remove(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<T> entities)
        {
            _context.Set<T>().RemoveRange(entities);
        }

        IEnumerable<T> IBaseRepository<T>.GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
