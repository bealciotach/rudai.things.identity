﻿using GraphQL;
using GraphQL.NewtonsoftJson;
using GraphQL.Types;
using Rudai.Things.Models.Shared;
using Rudai.Things.Schedules.Api.Core;
using Rudai.Things.Schedules.Api.Data;
using Rudai.Things.Schedules.Api.Models.GraphQL;
using System;
using System.Threading.Tasks;

namespace Rudai.Things.Schedules.Api.Services
{
    public class ScheduleRepository : BaseRepository<Schedule>, IScheduleRepository
    {
        protected readonly SchedulesContext _context;
        public ScheduleRepository(SchedulesContext context)
            :base (context)
        {
            _context = context;
        }

        public async Task<bool> AddSchedule(ScheduleInputType scheduleInput)
        {
            var schedule = BuildSchedule(scheduleInput);

            _context.Schedules.Add(schedule);
            bool added = await _context.SaveChangesAsync() > -1;
            return added;
        }

        public async Task<bool> UpdateSchedule(ScheduleInputType scheduleInput)
        {
            var schedule = BuildSchedule(scheduleInput);

            _context.Schedules.Update(schedule);
            bool updated = await _context.SaveChangesAsync() > -1;
            return updated;
        }

        public async Task<bool> UpdateState(ScheduleInputType scheduleInput)
        {
            bool actionComplete = false;
            var input = scheduleInput.GetField("action").GetPropertyValue("action").ToString();

            if (input == "add")
            {
                actionComplete = await AddSchedule(scheduleInput);
            }
            else if (input == "update")
            {
                actionComplete = await UpdateSchedule(scheduleInput);
            }

            return actionComplete;
        }

        private Schedule BuildSchedule(ScheduleInputType scheduleInput)
        {
            var schedule = new Schedule();
            schedule.ByDay = (string)scheduleInput.GetField("byday").GetValue();
            schedule.ByMonth = (string)scheduleInput.GetField("bymonth").GetValue();
            schedule.ByMonthDay = (string)scheduleInput.GetField("bymonthday").GetValue();
            schedule.ByMonthWeek = (string)scheduleInput.GetField("bymonthweek").ToString();
            schedule.Duration = (int)scheduleInput.GetField("duraton").GetValue();
            schedule.EndDate = (DateTime)scheduleInput.GetField("enddate").GetValue();
            schedule.EndTime = (DateTime)scheduleInput.GetField("endtime").GetValue();
            schedule.RepeatCount = (int)scheduleInput.GetField("repeatcount").GetValue();
            schedule.EndTime = (DateTime)scheduleInput.GetField("endtime").GetValue();
            schedule.ExceptDate = (DateTime)scheduleInput.GetField("exceptdate").GetValue();
            schedule.RepeatFrequency = (string)scheduleInput.GetField("repeatfrequency").GetValue();
            schedule.ScheduleTimezone = (string)scheduleInput.GetField("scheduletimezone").GetValue();

            return schedule;
        }
    }
}
