﻿using Rudai.Things.Models.Shared;
using Rudai.Things.Schedules.Api.Core;

namespace Rudai.Things.Schedules.Api.Services
{
    public interface IScheduleRepository : IBaseRepository<Schedule>
    {
    }
}
