﻿using GraphQL.Types;

namespace Rudai.Things.Schedules.Api.Models.GraphQL
{
    public class ScheduleInputType : InputObjectGraphType
    {
        public ScheduleInputType(): base()
        {
            Name = "ScheduleInput";
            Field<NonNullGraphType<StringGraphType>>("byday");
            Field<StringGraphType>("bymonth");
            Field<StringGraphType>("bymonthday");
            Field<StringGraphType>("bymonthweek");
            Field<IntGraphType>("duraton");
            Field<DateGraphType>("enddate");
            Field<DateTimeGraphType>("endtime");
            Field<DateGraphType>("exceptdate");
            Field<IntGraphType>("repeatcount");
            Field<IntGraphType>("repeatfrequency");
            Field<IntGraphType>("scheduletimezone");

            Field<StringGraphType>("action");
        }
    }
}