﻿

using GraphQL;
using GraphQL.NewtonsoftJson;
using GraphQL.Types;
using Rudai.Things.Schedules.Api.Helpers;
using Rudai.Things.Schedules.Api.Models.Graph;
using System;

namespace Rudai.Things.Schedules.Api.Models.GraphQL
{
    /// <summary>
    /// ScheduleTypeMutatIon ScheduleType MutatIons of ObjectGraphType
    /// </summary>
    public class ScheduleTypeMutatIon : ObjectGraphType
    {
        public ScheduleTypeMutatIon(ContextServiceLocator contextServiceLocator)
        {
            Name = "ScheduleTypeMutation";

            Field<ScheduleType>(
                "createSchedule",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<ScheduleInputType>> { Name = "schedule" }
                ),
                resolve: (context) =>
                {
                    var schedule = context.GetArgument<ScheduleInputType>("schedule");

                    var updated = contextServiceLocator.ScheduleRepository.UpdateState(schedule);
                    return updated.GetPropertyValue<bool>();
                });

            Field<ScheduleType>(
                "updateSchedule",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<ScheduleInputType>> { Name = "schedule" }
                ),
                resolve: context =>
                {
                    var schedule = context.GetArgument<ScheduleInputType>("schedule");

                    var updated = contextServiceLocator.ScheduleRepository.UpdateState(schedule);
                    return updated.GetPropertyValue<bool>();
                });

            Field<ScheduleType>(
                "deleteSchedule",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<GuidGraphType>> { Name = "schedule" }
                ),
                resolve: context =>
                {
                    var id = context.GetArgument<StringGraphType>("schedule");
                    var schedule = contextServiceLocator.ScheduleRepository.GetById(Guid.Parse(id.GetValue().ToString()));
                    contextServiceLocator.ScheduleRepository.Remove(schedule);
                    return true;
                });
        }
    }
}
