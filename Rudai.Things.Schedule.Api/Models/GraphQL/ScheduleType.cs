﻿using GraphQL.Types;
using Rudai.Things.Models.Shared;
using Rudai.Things.Schedules.Api.Helpers;
using Schema.NET;
using System;
using System.Linq;
using System.Text;

namespace Rudai.Things.Schedules.Api.Models.Graph
{
    /// <summary>
    /// ScheduleType ObjectGraphType of Schedule entity
    /// </summary>
    public class ScheduleType : ObjectGraphType<Schedule>
    {
        public ScheduleType(ContextServiceLocator contextServiceLocator)
        {
            Field<GuidGraphType>("id", resolve: context => context.Source.Id);
            Field(x => x.Action).Description("The Action - add or update"); 
            Field(x => x.Type).Description("The Type");
            Field(x => x.ByDay).Description("The ByDay").Resolve(context => GetDaysOfWeek(context.Source.ByDay));
            Field(x => x.ByMonth).Description("ByMonth");
            Field(x => x.ByMonthDay).Description("The ByMonthDay");
            Field(x => x.ByMonthWeek).Description("The ByMonthWeek");
            Field(x => x.Duration).Description("The Duration");
            Field(x => x.EndDate).Description("The EndDate");
            Field(x => x.EndTime).Description("The EndTime");
            Field(x => x.ExceptDate).Description("The ExceptDate");
            Field(x => x.RepeatCount).Description("The RepeatCount");
            Field(x => x.RepeatFrequency).Description("The RepeatFrequency");
            Field(x => x.ScheduleTimezone).Description("The ScheduleTimezone");
            Field(x => x.StartDate).Description("The StartDate");
            Field(x => x.StartTime).Description("The StartTime");

            //field<themetype>(
            //   "theme",
            //   arguments: new queryarguments(new queryargument<stringgraphtype> { name = "id" }),
            //   resolve: context => contextservicelocator.clientrepository.getclientbyid(guid.parse(context.getargument<string>("themeid")))
            //);
        }

        private string GetDaysOfWeek(string input)
        {
            string output = string.Empty;
            StringBuilder sb = new StringBuilder();
            var days = input.Split(",");
            for (int i = 0; i < days.Count(); i ++)
            {
                string day = Enum.GetName(typeof(Schema.NET.DayOfWeek), int.Parse(days[i]));
                sb.Append(day);
                sb.Append(" ");
            }

            output = sb.ToString();
            sb = null;

            return output;
        }
    }
}
