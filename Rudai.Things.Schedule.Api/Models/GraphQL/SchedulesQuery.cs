﻿using GraphQL.Types;
using Rudai.Things.Models.Shared;
using Rudai.Things.Schedules.Api.Helpers;

namespace Rudai.Things.Schedules.Api.Models.Graph
{
    /// <summary>
    /// ObjectGraphType ToursQuery
    /// </summary>
    /// <description>ToursQuery Query fields</description>
    public class SchedulesQuery : ObjectGraphType<Schedule>
    {
        public SchedulesQuery(ContextServiceLocator contextServiceLocator)
        {
            Field<ListGraphType<ScheduleType>>(
                "schedules",
                // arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "cursor" }),
                resolve: context => contextServiceLocator.ScheduleRepository.GetAll());


            //Field<ThemeType>(
            //    "theme",
            //    "A ClientTheme whose id matches",
            //    arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "id", Description = "" }),
            //    resolve: context => contextServiceLocator.ClientRepository.GetThemeByClientId(Guid.Parse(context.GetArgument<string>("id"))));

            //Field<ListGraphType<ThemeType>>(
            //    "themes",
            //    arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "cursor" }),
            //    resolve: context => contextServiceLocator.ClientRepository.GetAllThemes());

        }
    }
}


