﻿using GraphQL.Types;
using GraphQL.Utilities.Federation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Rudai.Things.Models.Shared;
using Rudai.Things.Schedules.Api.Core;
using Rudai.Things.Schedules.Api.Models.Graph;
using Rudai.Things.Schedules.Api.Models.GraphQL;
using Rudai.Things.Schedules.Api.Services;
using System;

namespace Rudai.Things.Schedules.Api.Config
{
    public static class SchedulesQLConfiguration
    {
        /// <summary>
        /// Configures the service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="configuration">The configuration.</param>
        public static void ConfigureService(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient(typeof(IBaseRepository<Schedule>), typeof(BaseRepository<Schedule>));          
            services.AddTransient<IScheduleRepository, ScheduleRepository>();

            // query mutation subscription
            services.AddTransient<SchedulesQuery>();
            services.AddTransient<ScheduleTypeMutatIon>();

            // types
            services.AddSingleton<ScheduleType>();
            services.AddTransient<ScheduleInputType>();

            // todo remove ExposeExceptions in prod
            // Register GraphQL services
            //services.AddGraphQL(options =>
            //{
            //    options.EnableMetrics = true;
            //    options.ExposeExceptions = true;
            //});
            // Register DepedencyResolver; this will be used when a GraphQL type needs to resolve a dependency
            // services.AddSingleton<IServiceProvider>(c => new FuncServiceProvider(type => c.GetRequiredService(type)));

            services.AddTransient<ISchema>(s =>
            {

                var repository = s.GetRequiredService<ScheduleRepository>();

                return FederatedSchema.For(@"
                        extend type Query {
                            Schedule(Id: Guid): Schedule
                            Schedules: [Schedule]
                        }
                    
                        type Schedule @key(fields: ""id"") {
                            id: Guid!
                            byDay: String
                            byMonth: String
                            byMonthDay: String
                            byMonthWeek: String,
                            endDate: Date,
                            endTime: Date,
                            exceptDate: Date,
                            repeatCount: Int,
                            repeatFrequency: Int,
                            scheduleTimezone: String,
                            startDate: Date,
                            startTime: Date,
                        }
                        
                    ", _ =>
                {
                    _.ServiceProvider = s;
                    _.Types.Include<SchedulesQuery>();
                    //_.Types.For("TouristAttraction").ResolveReferenceAsync(context =>
                    //{
                    //    Console.WriteLine("################ resolving TouristAttractions for Tour #############");
                    //    var clientId = (Guid)context.Arguments["id"];
                    //    Console.WriteLine("########### WHAT id " + clientId + "###############");

                    //    return repository.GetAll();

                    //});

                });
            });
        }
    }
}
