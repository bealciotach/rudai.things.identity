﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Rudai.Things.Schedules.Api.Config
{
    /// <summary>
    /// Configurations related to entity framework
    /// </summary>
    public static class EntityFrameworkConfiguration
    {
        /// <summary>
        /// Configures the service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="configuration">The configuration.</param>
        public static void ConfigureService(IServiceCollection services, IConfiguration configuration)
        {

            //services.AddTransient(typeof(IBaseRepository<Tour>), typeof(BaseRepository<Tour>));          
            //services.AddTransient<ITourRepository, TourRepository>();
        }

    }
}