﻿using Microsoft.EntityFrameworkCore;
using Rudai.Things.Models.Shared;

namespace Rudai.Things.Schedules.Api.Data
{
    public class SchedulesContext : DbContext
    {
        public SchedulesContext() : base()
        {

        }
        public SchedulesContext(DbContextOptions<SchedulesContext> options)
            :base(options)
        {

        }
        public DbSet<Schedule> Schedules { get; set; }
 
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

    }

}
