﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Rudai.Things.Clients.Configuration.Data;
using Rudai.Things.Clients.Configuration.Models;

namespace Rudai.Things.Clients.Configuration.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientConfigurationsController : ControllerBase
    {
        private readonly ClientConfigurationsContext _context;
        private readonly IClientRepository _repo;

        public ClientConfigurationsController(ClientConfigurationsContext context, IClientRepository repo)
        {
            _context = context;
            _repo = repo;
        }

        // GET: api/ClientConfigurations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Client>>> GetClientConfigurations()
        {
            return await _context.Clients.Include(cfg => cfg.ClientTheme).ToListAsync();
        }

        // GET: api/ClientConfigurations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Client>> GetClientConfiguration(Guid id)
        {
            var clientConfiguration = await _context.Clients.FindAsync(id);

            if (clientConfiguration == null)
            {
                return NotFound();
            }

            return clientConfiguration;
        }

        // PUT: api/ClientConfigurations/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutClientConfiguration(Guid id, Client clientConfiguration)
        {
            if (id != clientConfiguration.Id)
            {
                return BadRequest();
            }

            _context.Entry(clientConfiguration).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientConfigurationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ClientConfigurations
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Client>> PostClientConfiguration(Client clientConfiguration)
        {
            _context.Clients.Add(clientConfiguration);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetClientConfiguration", new { id = clientConfiguration.Id }, clientConfiguration);
        }

        // DELETE: api/ClientConfigurations/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Client>> DeleteClientConfiguration(Guid id)
        {
            var clientConfiguration = await _context.Clients.FindAsync(id);
            if (clientConfiguration == null)
            {
                return NotFound();
            }

            _context.Clients.Remove(clientConfiguration);
            await _context.SaveChangesAsync();

            return clientConfiguration;
        }

        private bool ClientConfigurationExists(Guid id)
        {
            return _context.Clients.Any(e => e.Id == id);
        }
    }
}
