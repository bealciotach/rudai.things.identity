﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Clients.Configuration.Models
{
    public class Client
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ClientTheme ClientTheme { get; set; }

    }
}
