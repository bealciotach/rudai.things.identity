﻿using Microsoft.EntityFrameworkCore;

namespace Rudai.Things.Clients.Configuration.Models
{
    public class ClientConfigurationsContext : DbContext
    {
        public ClientConfigurationsContext(DbContextOptions<ClientConfigurationsContext> options)
            :base(options)
        {

        }
        public DbSet<Client> Clients { get; set; }
        public DbSet<ClientTheme> ClientThemes { get; set; }
    }

}
