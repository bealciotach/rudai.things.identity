﻿using GraphQL.Types;
using Rudai.Things.Clients.Configuration.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Clients.Configuration.Models.GraphQl
{
    public class ThemeType : ObjectGraphType<ClientTheme>
    {
        public ThemeType(ContextServiceLocator contextServiceLocator)
        {
            Field<StringGraphType>("id", resolve: context => context.Source.Id);
            Field(x => x.Background).Description("The Background colour of the Theme");
            Field(x => x.OnPrimary).Description("The colour of text on OnPrimary of the Theme");
            Field(x => x.OnSecondary).Description("The colour of text on OnSecondary of the Theme");
            Field(x => x.OnSurface).Description("The colour of text on OnSurface of the Theme");
            Field(x => x.OnSurface).Description("The colour of text on OnSurface of the Theme");
            Field(x => x.Primary).Description("The Primary colour of the Theme");
            Field(x => x.Secondary).Description("The Secondary colour of the Theme");
            Field(x => x.ClientId).Description("The Secondary colour of the Theme");
        }
    }
}
