﻿using GraphQL;
using GraphQL.Types;
using Rudai.Things.Clients.Configuration.Helpers;
using Rudai.Things.Clients.Configuration.Models.GraphQl;
using System;

namespace Rudai.Things.Clients.Configuration.Models.Graph
{
    public class ClientConfigType : ObjectGraphType<Client>
    {
        public ClientConfigType(ContextServiceLocator contextServiceLocator)
        {
            Field<GuidGraphType>("id", resolve: context => context.Source.Id);
            Field(x => x.Name).Description("The Name of the Thing");
            Field(x => x.Description);
            Field(x => x.ClientTheme);

           // Field<ThemeType>(
           //   "theme",
           //   arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "id" }),
           //   resolve: context => contextServiceLocator.ClientRepository.GetClientById(Guid.Parse(context.GetArgument<string>("themeId")))
           //);
        }
    }
}
