﻿

using GraphQL;
using GraphQL.Types;
using Rudai.Things.Clients.Configuration.Helpers;

namespace Rudai.Things.Clients.Configuration.Models.Graph
{
    public class CreateClientConfigTypeMutation : ObjectGraphType
    {
        public CreateClientConfigTypeMutation(ContextServiceLocator contextServiceLocator)
        {
            Name = "CreateClientConfigTypeMutation";

            Field<ClientConfigType>(
                "createClient",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<ClientInputType>> { Name = "client" }
                ),
                resolve: context =>
                {
                    var client = context.GetArgument<Client>("client");
                    return contextServiceLocator.ClientRepository.AddClient(client);
                });

            Field<ClientConfigType>(
                "updateClient",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<ClientInputType>> { Name = "client" }
                ),
                resolve: context =>
                {
                    var client = context.GetArgument<Client>("client");
                    return contextServiceLocator.ClientRepository.UpdateClient(client);
                });

            Field<ClientConfigType>(
                "deleteClient",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<ClientInputType>> { Name = "client" }
                ),
                resolve: context =>
                {
                    var client = context.GetArgument<Client>("client");
                    return contextServiceLocator.ClientRepository.RemoveClient(client.Id);
                });
        }
    }
}
