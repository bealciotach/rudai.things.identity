﻿using GraphQL.Types;

namespace Rudai.Things.Clients.Configuration.Models.Graph
{
    public class ClientInputType : InputObjectGraphType
    {
        public ClientInputType()
        {
            Name = "ClientInput";
            Field<NonNullGraphType<StringGraphType>>("name");
            Field<StringGraphType>("description");
            Field<ThemeInputType>("theme", "The client theme");
        }
    }
}