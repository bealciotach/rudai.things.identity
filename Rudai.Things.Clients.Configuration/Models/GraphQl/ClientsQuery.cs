﻿
using GraphQL;
using GraphQL.Types;
using Rudai.Things.Clients.Configuration.Helpers;
using Rudai.Things.Clients.Configuration.Models;
using Rudai.Things.Clients.Configuration.Models.Graph;
using Rudai.Things.Clients.Configuration.Models.GraphQl;
using System;

namespace Rudai.InDerry.Api.Models
{
    /// <summary>
    /// ObjectGraphType ThingsQuery
    /// </summary>
    /// <description>Things Query fields</description>
    public class ClientsQuery : ObjectGraphType<Client>
    {
        public ClientsQuery(ContextServiceLocator contextServiceLocator)
        {
            Field<ClientConfigType>(
                "client",
                "A ClientConfigType whose id matches",
                arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "id" }),
                resolve: context => contextServiceLocator.ClientRepository.GetClientById(Guid.Parse(context.GetArgument<string>("id"))));


            Field<ListGraphType<ClientConfigType>>(
                "clients",
                // arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "cursor" }),
                resolve: context => contextServiceLocator.ClientRepository.GetAllClients());


            //Field<ThemeType>(
            //    "theme",
            //    "A ClientTheme whose id matches",
            //    arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "id", Description = "" }),
            //    resolve: context => contextServiceLocator.ClientRepository.GetThemeByClientId(Guid.Parse(context.GetArgument<string>("id"))));

            //Field<ListGraphType<ThemeType>>(
            //    "themes",
            //    arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "cursor" }),
            //    resolve: context => contextServiceLocator.ClientRepository.GetAllThemes());

        }
    }
}


