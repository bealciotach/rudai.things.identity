﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rudai.Things.Clients.Configuration.Models.Graph
{
    public class ThemeInputType : InputObjectGraphType
    {
        public ThemeInputType()
        {
            Name = "ThemeInput";
            Field<NonNullGraphType<StringGraphType>>("background");
            Field<NonNullGraphType<StringGraphType>>("onprimary");
            Field<NonNullGraphType<StringGraphType>>("onsecondary");
            Field<NonNullGraphType<StringGraphType>>("onsurface");
            Field<NonNullGraphType<StringGraphType>>("primary");
            Field<NonNullGraphType<StringGraphType>>("secondary");
            Field<NonNullGraphType<StringGraphType>>("surface");
            Field<NonNullGraphType<StringGraphType>>("clientid");

        }
    }
}
