﻿using System;

namespace Rudai.Things.Clients.Configuration.Models
{
    public class ClientTheme
    {
        public Guid Id { get; set; }

        /// <summary>
        /// The theme background color
        /// </summary>
        public string Background { get; set; }

        /// <summary>
        /// The theme primary color
        /// </summary>
        public string Primary { get; set; }

        /// <summary>
        /// The theme secondary color
        /// </summary>
        public string Secondary { get; set; }

        /// <summary>
        /// The theme surface color
        /// </summary>
        public string Surface { get; set; }

        /// <summary>
        /// Text color on top of a primary background
        /// </summary>
        public string OnPrimary { get; set; }

        /// <summary>
        /// Text color on top of a secondary background
        /// </summary>
        public string OnSecondary { get; set; }

        /// <summary>
        /// Text color on top of a surface background
        /// </summary>
        public string OnSurface { get; set; }

        public Guid ClientId { get; set; }
        public Client Client { get; set; }

    }
}