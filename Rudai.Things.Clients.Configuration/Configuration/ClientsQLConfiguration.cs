﻿using GraphQL;
using GraphQL.Types;
using GraphQL.Utilities.Federation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Rudai.InDerry.Api.Models;
using Rudai.Things.Clients.Configuration.Data;
using Rudai.Things.Clients.Configuration.Models.Graph;
using Rudai.Things.Clients.Configuration.Models.GraphQl;
using System;

namespace Rudai.Things.Clients.Configuration.Config
{
    public static class ClientsQLConfiguration
    {
        /// <summary>
        /// Configures the service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="configuration">The configuration.</param>
        public static void ConfigureService(IServiceCollection services, IConfiguration configuration)
        {

            // query mutation subscription
            services.AddSingleton<ClientsQuery>();
            services.AddSingleton<CreateClientConfigTypeMutation>();

            // types
            services.AddSingleton<ClientConfigType>();
            services.AddSingleton<ThemeType>();

            // todo remove ExposeExceptions in prod
            // Register GraphQL services
            //services.AddGraphQL(options =>
            //{
            //    options.EnableMetrics = true;
            //    options.ExposeExceptions = true;
            //});
            // Register DepedencyResolver; this will be used when a GraphQL type needs to resolve a dependency
            // services.AddSingleton<IServiceProvider>(c => new FuncServiceProvider(type => c.GetRequiredService(type)));

            services.AddTransient<ISchema>(s =>
            {

                var clientRepository = s.GetRequiredService<IClientRepository>();

                return FederatedSchema.For(@"
                        extend type Query {
                            Client(Id: Guid): Client
                            Clients: [Client]
                        }
                    
                        type Client @key(fields: ""id"") {
                            id: Guid!
                            description: String
                            name: String
                        }

                        extend type ClientTheme @key(fields: ""id""){
                            id: Guid! @external
                            background: String
                            onPrimary: String
                            onSecondary: String
                            onSurface: String
                            primary: String
                            secondary: String
                            surface: String
                            description: String
                        }
                        
                    ", _ =>
                {
                    _.ServiceProvider = s;
                    _.Types.Include<ClientsQuery>();
                    _.Types.Include<CreateClientConfigTypeMutation>();
                    _.Types.For("ClientConfigType").ResolveReferenceAsync(context =>
                    {
                        Console.WriteLine("################ resolving ClientConfigType for Client #############");
                        var clientId = (Guid)context.Arguments["id"];
                        Console.WriteLine("########### ClientConfigType id " + clientId + "###############");

                        return clientRepository.GetClientById(clientId);

                    });
                    _.Types.For("ThemeType").ResolveReferenceAsync(context =>
                    {
                        Console.WriteLine("################ resolving ClientConfigType for Client #############");
                        var clientId = (Guid)context.Arguments["id"];
                        Console.WriteLine("########### ThemeType id " + clientId + "###############");

                        return clientRepository.GetThemeByClientId(clientId);

                    });

                });
            });
        }
    }
}
