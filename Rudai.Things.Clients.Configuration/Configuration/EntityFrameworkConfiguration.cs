﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Rudai.Things.Clients.Configuration.Core;
using Rudai.Things.Clients.Configuration.Data;
using Rudai.Things.Clients.Configuration.Models;

namespace Rudai.Things.Clients.Configuration.Config
{
    /// <summary>
    /// Configurations related to entity framework
    /// </summary>
    public static class EntityFrameworkConfiguration
    {
        /// <summary>
        /// Configures the service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="configuration">The configuration.</param>
        public static void ConfigureService(IServiceCollection services, IConfiguration configuration)
        {

            services.AddScoped<IClientRepository, ClientRepository>();
            services.AddTransient(typeof(IBaseRepository<Client>), typeof(BaseRepository<Client>));
            services.AddTransient<IClientRepository, ClientRepository>();

        }
    }
}