﻿
using Rudai.Things.Clients.Configuration.Core;
using Rudai.Things.Clients.Configuration.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Rudai.Things.Clients.Configuration.Data
{
    public interface IClientRepository
    {
        Task<Client> GetClientById(Guid id);
        Task<IEnumerable<Client>> GetAllClients();
        //Task<IEnumerable<Client>> Find(Expression<Func<Client, bool>> expression);
        Task<Client> AddClient(Client entity);
        Task<Client> UpdateClient(Client entity);
        Task<int> RemoveClient(Guid id);

        void AddClientRange(IEnumerable<Client> entities);

        void RemoveClientRange(IEnumerable<Client> entities);



        Task<ClientTheme> GetThemeById(Guid id);
        Task<ClientTheme> GetThemeByClientId(Guid id);
        Task<IEnumerable<ClientTheme>> GetAllThemes();
        //Task<IEnumerable<Client>> Find(Expression<Func<Client, bool>> expression);
        Task<ClientTheme> AddClientTheme(ClientTheme entity);
        Task<ClientTheme> UpdateClientTheme(ClientTheme entity);

        void AddClientThemeRange(IEnumerable<ClientTheme> entities);
        void RemoveClientTheme(Guid id);
        void RemoveClientThemeRange(IEnumerable<ClientTheme> entities);

    }
}
