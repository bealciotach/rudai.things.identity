﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Rudai.Things.Clients.Configuration.Core;
using Rudai.Things.Clients.Configuration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Rudai.Things.Clients.Configuration.Data
{
    public class ClientRepository : IClientRepository
    {
        protected readonly ClientConfigurationsContext _context;

        public ClientRepository(ClientConfigurationsContext context)
        {
            _context = context;
        }
        public async Task<Client> AddClient(Client entity)
        {
            var added = await _context.Clients.AddAsync(entity);
            await _context.SaveChangesAsync();
            return added.Entity;
        }

        public async void AddClientRange(IEnumerable<Client> entities)
        {
            _context.Clients.AddRange(entities);
            await _context.SaveChangesAsync();
        }

        public async void Remove(Guid id)
        {
            _context.Remove(id);
            await _context.SaveChangesAsync();
        }

        public async void RemoveClientRange(IEnumerable<Client> entities)
        {
            _context.RemoveRange(entities);
            await _context.SaveChangesAsync();
        }

        public async Task<Client> GetClientById(Guid id)
        {
            var clientConfiguration = await _context.Clients.FindAsync(id);
            return clientConfiguration;
        }


        public async Task<IEnumerable<Client>> GetAllClients()
        {
            return await _context.Clients.Include(cfg => cfg.ClientTheme).ToListAsync();
        }

        public async Task<Client> UpdateClient(Client entity)
        {
            var updatedItem = _context.Clients.Update(entity);
            await _context.SaveChangesAsync();
            return updatedItem.Entity;
        }

        public async Task<int> RemoveClient(Guid id)
        {
            int removed = 0;
            var client = await GetClientById(id);
            var updatedItem = _context.Clients.Remove(client);
            try
            {
                await _context.SaveChangesAsync();
                removed = 1;
            }
            catch (Exception)
            {

                throw;
            }
            return removed;
        }

        public async Task<ClientTheme> GetThemeById(Guid id)
        {
            var theme = await _context.ClientThemes.FindAsync(id);
            return theme;
        }

        public async Task<ClientTheme> GetThemeByClientId(Guid id)
        {
            var theme = await _context.ClientThemes.Where(t => t.Id == id).FirstAsync();
            return theme;
        }

        public async Task<IEnumerable<ClientTheme>> GetAllThemes()
        {
            return await _context.ClientThemes.Include(cfg => cfg.Client).ToListAsync();
        }

        public async Task<ClientTheme> AddClientTheme(ClientTheme entity)
        {
            var added = await _context.ClientThemes.AddAsync(entity);
            await _context.SaveChangesAsync();
            return added.Entity;
        }

        public async Task<ClientTheme> UpdateClientTheme(ClientTheme entity)
        {
            var updatedItem = _context.ClientThemes.Update(entity);
            await _context.SaveChangesAsync();
            return updatedItem.Entity;
        }

        public async void AddClientThemeRange(IEnumerable<ClientTheme> entities)
        {
            _context.ClientThemes.AddRange(entities);
            await _context.SaveChangesAsync();
        }

        public async void RemoveClientTheme(Guid id)
        {
            var client = await GetThemeById(id);
            var updatedItem = _context.ClientThemes.Remove(client);
            await _context.SaveChangesAsync();
        }

        public async void RemoveClientThemeRange(IEnumerable<ClientTheme> entities)
        {
            _context.ClientThemes.RemoveRange(entities);
            await _context.SaveChangesAsync();
        }
    }
}
