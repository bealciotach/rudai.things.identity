﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Rudai.Things.Models.Shared
{
    public class BookingValidationModel
    {
        [Required(ErrorMessage = "ID associated with this booking is required")]
        public Guid Id { get; set; }
        public Guid BookingId { get; set; }
        [MinLength(10, ErrorMessage = "minimum of 10")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Your Tour must have a title")]
        public string TourTitle { get; set; }

        [MinLength(10, ErrorMessage = "minimum of 10")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Your Tour must have a title")]
        public string Title { get; set; }

        [MinLength(10, ErrorMessage = "minimum of 10")]
        [Required(ErrorMessage = "Description of this tour is required")]
        public string Description { get; set; }

        [Required]
        public DateTime BookingDate { get; set; }

        public static ValidationResult RequiredDateTime(DateTime value, ValidationContext vc)
        {
            return value > DateTime.MinValue
                ? ValidationResult.Success
                : new ValidationResult($"The {vc.MemberName} field is required.", new[] { vc.MemberName });
        }

    }
}
