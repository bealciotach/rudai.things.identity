﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rudai.Things.Models.Shared
{
    public class Booking
    {
        public Guid Id { get; set; }
        //public DateTime Date { get; set; }

        public string Title { get; set; }
        public DateTime BookingDate { get; set; }

        public string Description { get; set; }
        public Booking(Guid id, string title, string description, DateTime bookingDate) =>
            (Id, Title, Description, BookingDate) = (id, title, description, bookingDate);
    }
}
