﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Schema.NET;

namespace Rudai.Things.Models.Shared
{
    public class Schedule // : Schema.NET.Thing
    {

        public Guid Id { get; set; }

        /// <summary>
        /// Gets the name of the type as specified by schema.org.
        /// </summary>
        public string Type { get; private set; } = "Schedule";
#nullable enable
        /// <summary>
        /// ByDay Defines the day(s) of the week on which a recurring Event takes place. 
        /// May be specified using either DayOfWeek, or alternatively Text conforming to iCal's syntax for byDay recurrence rules
        /// Sunday = 0, Monday = 2... PublicHolidays = 7
        /// </summary>
        public string? ByDay { get; set; }

        /// <summary>
        /// ByMonth Defines the month(s) of the year on which a recurring Event takes place. Specified as an Integer between 1-12.
        /// January is 1.        
        /// /// February is 2.        
        /// /// December is is 12.
        /// </summary>
        /// 
        ///s[Range(1, 12)]
        public string? ByMonth { get; set; }

        /// <summary>
        /// Defines the day(s) of the month on which a recurring Event takes place. Specified as an Integer between 1-31.
        /// </summary>
        public string? ByMonthDay { get; set; }

        /// <summary>
        /// Defines the week(s) of the month on which a recurring Event takes place. Specified as an Integer between 1-5. 
        /// For clarity, byMonthWeek is best used in conjunction with byDay to indicate concepts like the first and third Mondays of a month.
        /// </summary>
        public string? ByMonthWeek { get; set; }

        public int? Duration { get; set; }

        /// <summary>
        /// The end date and time of the item (in ISO 8601 date format).
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// The endTime of something. For a reserved event or service (e.g. FoodEstablishmentReservation), the time that it is expected to end. 
        /// For actions that span a period of time, when the action was performed. e.g. John wrote a book from January to December. 
        /// For media, including audio and video, it's the time offset of the end of a clip within a larger file.
        /// Note that Event uses startDate/endDate instead of startTime/endTime, even when describing dates with times.This situation may be clarified in future revisions
        /// </summary>
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// Defines a Date or DateTime during which a scheduled Event will not take place. The property allows exceptions to a Schedule to be specified. 
        /// If an exception is specified as a DateTime then only the event that would have started at that specific date and time should be excluded from the schedule. 
        /// If an exception is specified as a Date then any event that is scheduled for that 24 hour period should be excluded from the schedule. 
        /// This allows a whole day to be excluded from the schedule without having to itemise every scheduled event.
        /// </summary>
        public DateTime? ExceptDate { get; set; }

        /// <summary>
        /// Defines the number of times a recurring Event will take place
        /// </summary>
        public int? RepeatCount { get; set; }

        /// <summary>
        /// TODO Defines the frequency at which Events will occur according to a schedule Schedule. The intervals between events should be defined as a Duration of time.
        /// </summary>
        public string? RepeatFrequency { get; set; }

        /// <summary>
        /// Indicates the timezone for which the time(s) indicated in the Schedule are given. The value provided should be among those listed in the IANA Time Zone Database.
        /// </summary>
        public string? ScheduleTimezone { get; set; }

        /// <summary>
        /// The start date and time of the item (in ISO 8601 date format).
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// The startTime of something. For a reserved event or service (e.g. FoodEstablishmentReservation), the time that it is expected to start.
        /// For actions that span a period of time, when the action was performed. e.g. John wrote a book from January to December.
        /// For media, including audio and video, it's the time offset of the start of a clip within a larger file.

        /// Note that Event uses startDate/endDate instead of startTime/endTime, even when describing dates with times.This situation may be clarified in future revisions.
        /// </summary>
        public DateTime? StartTime { get; set; }
#nullable disable

        [NotMapped]
        public string Action { get; set; }
    }
}
