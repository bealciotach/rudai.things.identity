﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rudai.Things.Models.Shared
{
    public class Tour
    {
        public Guid Id { get; set; }
        //public DateTime Date { get; set; }

        public string Title { get; set; }
        public DateTime TourDate { get; set; }

        public string Description { get; set; }
        public Tour(Guid id, string title, string description, DateTime tourDate) => 
            (Id, Title, Description, TourDate) = (id, title, description, tourDate);

        public List<Schedule> SchedulesList { get; set; }

    }
}
