﻿using System;
using System.ComponentModel.DataAnnotations;

using System.Collections.Generic;
using System.Text;

namespace Rudai.Things.Models.Shared
{
    public static class RequiredDateValidation
    {
        public static ValidationResult RequiredDateTime(DateTime value, ValidationContext vc)
        {
            return value > DateTime.MinValue
                ? ValidationResult.Success
                : new ValidationResult($"The {vc.MemberName} field is required.", new[] { vc.MemberName });
        }
    }
}
