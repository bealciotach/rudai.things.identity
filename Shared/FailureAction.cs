﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rudai.Things.Models.Shared.Actions
{
    public abstract class FailureAction
    {
        public string ErrorMessage { get; }
        protected FailureAction(string errorMessage) =>
            ErrorMessage = errorMessage;
    }
}
